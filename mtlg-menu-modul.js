/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-07-27T13:10:43+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   Rabea
 * @Last modified time: 2019-07-03T18:33:14+02:00
 */



"use strict";

/**
 * @namespace menu
 * @memberof MTLG
 *
 **/

// required file(s)
require('./menu.css');


// options
var edgesToClick; // how many edges to click to open the menu
var _clickedEdges; // var to count how many edges are clicked
var rotateAngle; // var to rotate the elements in the circle
var timerCount; // timer till the game buttons reset
var mode; // HTML menu (0) or canvas menu (1)

var menuContainer;
var menuContent;
var _gameMenuButtons = []; // array of the game buttons to open the menu
var _gameMenuButtonsCanvas = []; // array of the game buttons to open the menu
var _menuOpen;
var edges;
var _zoll;
var _gameButtonTimer; // Timer for gameMenuButtons
var _stage; // stage vor canvas menu
var _menuStage; // menu stage
var _menuContainer; // container for canvas menu entries
var _menuButtonContainer; // container for canvas game buttons

var initalized = false;

// Private functions

/////////////////////////////
// HTML
////////////////////////////

function _createOverlay() {
  var o = document.getElementById('tangible_overlay') || document.getElementById('canvasObject');

  menuContainer = document.createElement("div");
  menuContent = document.createElement("div");

  menuContainer.setAttribute('id', 'overlayMenu');
  o.parentNode.insertBefore(menuContainer, o.nextSibling);

  menuContent.setAttribute('id', 'menuContent');
  menuContainer.appendChild(menuContent);
}

function _addMenuEntryPic(path, callback) {
  var entry = document.createElement("img");
  entry.setAttribute('class', 'menuEntryImg');
  entry.src = path;
  entry.addEventListener('click', callback);
  menuContent.appendChild(entry);
}

/*
 * Adds a menu entry as text. It is possible to choose wheter it is displayed as one to four words. Default words are 4.
 * @param {type} text     The text of the menu entry.
 * @param {type} callback The callback function of the menu entry.
 * @param {type} edges    It is possible to choose between 1 to 4 words for a menu entry.
 * @return {type} Description
 */
function _addMenuEntryText(text, callback, edges) {
  var entry = document.createElement("div");
  var textArray = [];
  var edgeArray = ['textTop', 'textDown', 'textLeft', 'textRight'];
  var edgeConstMiddle = [-0.35, -0.80, -0.70, -1.70];
  var edgeConstSmall = [-0.25, -0.95, -0.80, -1.80];
  var i;
  var textBounding;

  edges = edges || 4;

  entry.setAttribute('class', 'menuEntryText');


  for (i = 0; i < edges; i++) {
    textArray.push(document.createElement("div"));
    textArray[i].innerHTML = text;
    if (edges > 1) {
      textArray[i].setAttribute('id', edgeArray[i]);
    }
    textArray[i].addEventListener('touchstart', callback);
    entry.appendChild(textArray[i]);
  }

  menuContent.appendChild(entry);

  if (edges > 1) {
    textBounding = textArray[0].getBoundingClientRect().width;

    if (textBounding < 70) {
      for (i = 0; i < edges; i++) {
        _styleEdge(textArray[i], textBounding * edgeConstSmall[i], i);
      }
    } else {
      for (i = 0; i < edges; i++) {
        _styleEdge(textArray[i], textBounding * edgeConstMiddle[i], i);
      }
    }
  }

  function _styleEdge(obj, constStyle, edge) {
    switch (edge) {
      case 0:
        obj.style.top = constStyle + "px";
        break;
      case 1:
        obj.style.bottom = constStyle + "px";
        break;
      case 2:
        obj.style.left = constStyle + "px";
        break;
      case 3:
        obj.style.right = constStyle + "px";
        break;
      default:
        console.log("function _styleEdge no or false input");
    }
  }
}

function _addMenuGameButtons() {
  var o = document.getElementById('overlayMenu');
  var i;

  for (i = 0; i < edges; i++) {
    _gameMenuButtons.push(document.createElement("div"));
    _gameMenuButtons[i].setAttribute('class', 'gameMenuButtons');
    _gameMenuButtons[i].addEventListener('click', _handleGameMenu);
    o.parentNode.insertBefore(_gameMenuButtons[i], o.nextSibling);
  }

  _setPositionGameButtons(edges);
  //_resetMenuGameButtons();
}

function _setPositionGameButtons(edg) {
  var posEdges = ["ol", "or", "ul", "ur"];
  var i;

  for (i = 0; i < edg; i++) {
    _gameMenuButtons[i].className += " " + posEdges[i];
  }
}

function _toggleMenuEntries(sw) {
  var entries = menuContainer.children;
  var i;
  var modus;

  sw ? modus = "hidden" : modus = "visible";

  for (i = 0; i < entries.length; i = i + 1) {
    entries[i].style.visibility = modus;
  }
}

//
function _groupMenuEntries() {
  var screenH = window.innerHeight;
  var screenW = window.innerWidth;
  var p, i;

  var btn = menuContent.children;

  for (i = 0; i < btn.length; i = i + 1) {
    p = _circle(i + 1, 15, btn.length, rotateAngle);
    btn[i].style.top = p.x + "%";
    btn[i].style.left = p.y + "%";
    btn[i].style.transform = "rotate(" + p.rotation + "deg)";
    //console.log(p.x + " " + p.y);
  }
}


//////////////////////////////
// utilities
//////////////////////////////

/*
 * calculate a position on a given circle
 * @param {type} pos           The position of the element 0...n.
 * @param {type} radius        The radius of the circle.
 * @param {type} nbrOfelements This factor is the number of elements to place on the circle.
 * @param {type} rotFak        This factor rotate the position in the circle in PI / rotFak.
 * @return {type} Returns a Point with x, y and the rotation depending on the position and the middle of the circle.
 */
function _circle(pos, radius, nbrOfelements, rotFak) {
  var alpha = pos * Math.PI * 2 / nbrOfelements;
  var _p = new p();
  var fak;

  rotFak ? fak = Math.PI / rotFak : fak = 0;

  function p() {
    this.x = 0;
    this.y = 0;
    this.rotation = 0;
  }

  _p.x = Math.cos(alpha + fak) * radius + 50;
  _p.y = Math.sin(alpha + fak) * radius + 50;
  _p.rotation = (alpha + fak) * -180 / Math.PI;

  return _p;
}


/*
 * calculate a position on a given circle
 * @param {type} pos           The position of the element 0...n.
 * @param {type} radius        The radius of the circle.
 * @param {type} nbrOfelements This factor is the number of elements to place on the circle.
 * @param {type} rotFak        This factor rotate the position in the circle in PI / rotFak.
 *
 * @return {type} Returns a Point with x, y and the rotation depending on the position and the middle of the circle.
 */
function _circleCanvas(pos, radius, nbrOfelements, rotFak) {
  var alpha = pos * Math.PI * 2 / nbrOfelements;
  var _p = new p();
  var fak;

  rotFak ? fak = Math.PI / rotFak : fak = 0;

  function p() {
    this.x = 0;
    this.y = 0;
    this.rotation = 0;
  }

  _p.x = Math.cos(alpha + fak) * radius + 50;
  _p.y = Math.sin(alpha + fak) * radius + 50;

  //rotation einfügen
  let ankathete = _p.y - radius / 2;
  let gegenkathete = _p.x - radius / 2;
  if (_p.y > radius / 2) {
    var degree = (Math.atan(-gegenkathete / ankathete) * 180 / Math.PI) % 360;
  } else {
    var degree = ((Math.atan(-gegenkathete / ankathete) * 180 / Math.PI) - 180) % 360;
  }

  _p.rotation = degree;

  return _p;
}


// Timer function
// https://stackoverflow.com/questions/3969475/javascript-pause-settimeout
function Timer(callback, delay) {
  var timerId, start, remaining = delay;

  this.pause = function() {
    window.clearTimeout(timerId);
    remaining -= new Date() - start;
  };

  this.resume = function() {
    start = new Date();
    window.clearTimeout(timerId);
    timerId = window.setTimeout(callback, remaining);
  };

  this.resume();
}

function _insertAfter(newNode, referenceNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

////////////////////////////////
// Canvas
////////////////////////////////

function _groupMenuEntriesCanvas() {
  for (var i = 0; i < _menuContainer.children.length; i++) {
    if (_menuContainer.children[i].name !== 'bgShape') {
      let p = _circleCanvas(i + 1, 200, _menuContainer.children.length - 1, rotateAngle);
      _menuContainer.children[i].x = p.x + MTLG.getOptions().width / 2 - 100;
      _menuContainer.children[i].y = p.y + MTLG.getOptions().height / 2 - 100;
      _menuContainer.children[i].rotation = p.rotation;

    }
  }
}

function _addMenuEntryPicCanvas(img, callback, size) {
  let c = new createjs.Container();

  let imageObject = document.createElement("IMG");
  imageObject.setAttribute('src', img);
  let source = new createjs.Bitmap(imageObject);

  if (!source.getBounds()) {
    let _size = size || 35;
    source.setBounds(0, 0, _size, _size);
  }
  let fak = source.getBounds().width;
  let scaleFak = 100 / fak;
  c.scaleX = c.scaleY = scaleFak;


  let bg = new createjs.Shape();
  bg.graphics.beginFill("rgba(0, 0, 0, 0.9)").drawCircle(0, 0, source.getBounds().height / 2);
  bg.regX = -source.getBounds().width / 2;
  bg.regY = -source.getBounds().height / 2;

  c.on('click', callback);

  c.x = c.y = 200;
  c.addChild(bg, source);
  c.regX = source.getBounds().width / 2;
  c.regY = source.getBounds().height / 2;
  _menuContainer.addChild(c);
}

function _renderCanvasMenu(o) {
  let opt = o || {};
  let bgColor = opt.bgColor || "rgba(125, 97, 68, 0.7)";
  let stColor = opt.stColor || "rgba(125, 97, 68, 0.7)";
  let stStyle = opt.stStyle || 0;
  let radius = opt.radius || 50;

  _menuButtonContainer = new createjs.Container();

  function menuButton(posX, posY, posMenu) {
    let pX = posX || 0;
    let pY = posY || 0;
    this.c = new createjs.Shape();
    this.c.graphics.setStrokeStyle(stStyle).beginStroke(stColor).beginFill(bgColor).moveTo(0, 0).arc(0, 0, radius, posMenu * Math.PI / 2, (posMenu + 1) * Math.PI / 2);
    this.c.x = pX;
    this.c.y = pY;
    this.c.on('click', _handleGameMenu);
    return this.c;
  }

  _gameMenuButtonsCanvas.push(new menuButton(0, 0, 0));
  _gameMenuButtonsCanvas.push(new menuButton(MTLG.getOptions().width, 0, 1))
  _gameMenuButtonsCanvas.push(new menuButton(MTLG.getOptions().width, MTLG.getOptions().height, 2))
  _gameMenuButtonsCanvas.push(new menuButton(0, MTLG.getOptions().height, 3))

  for (var i = 0; i < 4; i++) {
    _menuButtonContainer.addChild(_gameMenuButtonsCanvas[i]);
  }

  _menuContainer = new createjs.Container();
  _menuContainer.visible = false;
  let menuBG = new createjs.Shape();
  menuBG.graphics.beginFill("rgba(0, 0, 0, 0.9)").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
  menuBG.name = "bgShape";

  _menuContainer.addChild(menuBG);

  _menuStage = new createjs.Stage(_addMenuCanvas());
  createjs.Touch.enable(_menuStage);
  _menuStage.nextStage = _stage;
  _menuStage.addChild(_menuButtonContainer, _menuContainer);
  createjs.Ticker.addEventListener("tick", _menuStage);
}

function _addMenuCanvas() {
  let newNode = _stage.canvas.cloneNode(true);
  newNode.setAttribute('id', 'menu');
  _insertAfter(newNode, _stage.canvas);
  return newNode;
}

////////////////////////////////
// Menu Functions
////////////////////////////////


function _resetMenuGameButtons(obj) {
  var i;
  _clickedEdges = 0;

  if (!_menuOpen) {

    for (i = 0; i < edges; i++) {
      createjs.Tween.get(_gameMenuButtonsCanvas[i], {}).to({
        scaleX: 1,
        scaleY: 1
      }, 1000, createjs.Ease.get(1));;
    }
  }
}

// handler

function _handleMenuEntries() {

}

function _handleGameMenu() {
  _clickedEdges += 1;
  _gameButtonTimer.resume();

  if (_clickedEdges === edgesToClick) {
    _gameButtonTimer.pause();
    openMenu();
  }

  createjs.Tween.get(this, {}).to({
    scaleX: 0,
    scaleY: 0
  }, 1000, createjs.Ease.get(1));
}



function _addBasicMenuEntries() {
  _addMenuEntryPicCanvas(exitIMG(), function() {
    console.log("Return to start screen. Maybe to games overview.");
    MTLG.lc.goToMenu();
    closeMenu();
  }, 32);
  _addMenuEntryPicCanvas(closeIMG(), closeMenu, 512);
}


////////////////////////////
// images
///////////////////////////

function settingIMG() {
  return "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAtMSAyOCAyOSIgaGVpZ2h0PSIyOXB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgLTEgMjggMjkiIHdpZHRoPSIyOHB4IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48ZGVmcy8+PHBhdGggY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNMjYuMSwyMC45YzEuNiwxLjYsMS42LDQuMSwwLDUuN3MtNC4xLDEuNi01LjcsMGwtOC41LTguNCAgYy0wLjItMC4yLTAuNC0wLjUtMC42LTAuOGMtMy4yLDAuOS02LjctMC4xLTguOS0yLjhsLTAuNS0wLjdDLTAuMywxMS4yLTAuNiw3LjUsMSw0LjZjMCwwLDAsMC4xLDAuMSwwLjFsMy45LDUgIGMxLjIsMS41LDMuMywxLjcsNC44LDAuNWMxLjUtMS4yLDEuNy0zLjMsMC41LTQuOGwtMy45LTVjMCwwLTAuMS0wLjEtMC4xLTAuMWMzLjItMC45LDYuNywwLjEsOC45LDIuOGwwLjUsMC43ICBjMiwyLjQsMi40LDUuNiwxLjQsOC4zYzAuMiwwLjEsMC4zLDAuMiwwLjUsMC40TDI2LjEsMjAuOXogTTIzLjMsMjUuM2MwLjgsMCwxLjQtMC42LDEuNC0xLjRjMC0wLjgtMC42LTEuNC0xLjQtMS40ICBjLTAuOCwwLTEuNCwwLjYtMS40LDEuNEMyMS45LDI0LjYsMjIuNSwyNS4zLDIzLjMsMjUuM3oiIGZpbGw9IiNGMEYwRjAiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==";
}

function trashIMG() {
  return "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+DQo8c3ZnDQogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iDQogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIg0KICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIg0KICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyINCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyINCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCINCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIg0KICAgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAtMC41IC0wLjcgMzEgMzIiDQogICBoZWlnaHQ9IjMycHgiDQogICB2ZXJzaW9uPSIxLjEiDQogICB2aWV3Qm94PSItMC41IC0wLjcgMzEgMzIiDQogICB3aWR0aD0iMzFweCINCiAgIHhtbDpzcGFjZT0icHJlc2VydmUiDQogICBpZD0ic3ZnMTU5Ig0KICAgc29kaXBvZGk6ZG9jbmFtZT0iaWZfVmVjdG9yLWljb25zXzQyXzEwNDE2NTAuc3ZnIg0KICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi4yICg1YzNlODBkLCAyMDE3LTA4LTA2KSI+PG1ldGFkYXRhDQogICAgIGlkPSJtZXRhZGF0YTE2MyI+PHJkZjpSREY+PGNjOldvcmsNCiAgICAgICAgIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZQ0KICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48c29kaXBvZGk6bmFtZWR2aWV3DQogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiINCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiDQogICAgIGJvcmRlcm9wYWNpdHk9IjEiDQogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiDQogICAgIGdyaWR0b2xlcmFuY2U9IjEwIg0KICAgICBndWlkZXRvbGVyYW5jZT0iMTAiDQogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIg0KICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIg0KICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjExMTgiDQogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9Ijc0NiINCiAgICAgaWQ9Im5hbWVkdmlldzE2MSINCiAgICAgc2hvd2dyaWQ9ImZhbHNlIg0KICAgICBpbmtzY2FwZTp6b29tPSIxMC40Mjk4MjUiDQogICAgIGlua3NjYXBlOmN4PSIxNy45NzY1NDEiDQogICAgIGlua3NjYXBlOmN5PSIxNS40NTk5NTMiDQogICAgIGlua3NjYXBlOndpbmRvdy14PSIxNDc3Ig0KICAgICBpbmtzY2FwZTp3aW5kb3cteT0iMTU5Ig0KICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIwIg0KICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmcxNTkiIC8+PGRlZnMNCiAgICAgaWQ9ImRlZnMxNTUiIC8+PHBhdGgNCiAgICAgY2xpcC1ydWxlPSJldmVub2RkIg0KICAgICBkPSJNMjkuOCw4LjNDMjkuOCw4LjMsMjkuOCw4LjMsMjkuOCw4LjNMMjkuOCw4LjMgIEMyOS44LDguMywyOS44LDguMywyOS44LDguM3ogTTI3LjgsMTAuM2gtMXYxOGMwLDEuMS0wLjksMi0yLDJoLTE5Yy0xLjEsMC0yLTAuOS0yLTJ2LTE4SDJjLTEuMSwwLTItMC45LTItMmMwLTEuMSwwLjktMiwyLTJoNi45ICBDOS4yLDIuOCwxMiwwLDE1LjQsMHM2LjMsMi44LDYuNSw2LjNoNS44YzEuMSwwLDIsMC45LDIsMkMyOS44LDkuNCwyOC45LDEwLjMsMjcuOCwxMC4zeiBNMTUuNCwzLjJjLTEuOCwwLTMuMiwxLjMtMy40LDMuMWg2LjkgIEMxOC42LDQuNiwxNy4yLDMuMiwxNS40LDMuMnogTTIzLjgsMTEuNWMwLTAuNS0wLjItMC45LTAuNS0xLjFINy4zQzcsMTAuNiw2LjgsMTEsNi44LDExLjV2MTQuN2MwLDAuOSwwLjcsMS42LDEuNiwxLjZoMTMuOCAgYzAuOSwwLDEuNi0wLjcsMS42LTEuNlYxMS41eiBNMTguOCwxMi4zaDN2MTRoLTNWMTIuM3ogTTEzLjgsMTIuM2gzdjE0aC0zVjEyLjN6IE04LjgsMTIuM2gzdjE0aC0zVjEyLjN6IE0wLDguM0MwLDguMywwLDguMywwLDguMyAgQzAsOC4zLDAsOC4zLDAsOC4zTDAsOC4zeiINCiAgICAgZmlsbD0iIzBEMEQwRCINCiAgICAgZmlsbC1ydWxlPSJldmVub2RkIg0KICAgICBpZD0icGF0aDE1NyIgLz48cGF0aA0KICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZS13aWR0aDowLjEzNTU5MzIyIg0KICAgICBkPSJNIDUuNDcwNDY1NCwzMC43MjAzNjkgQyA1LjIxMTg0ODQsMzAuNTgxMjM4IDQuODc2MjU1MiwzMC4zMDEzNTUgNC43MjQ3MDI2LDMwLjA5ODQwNSA0LjQ1NDc1NTQsMjkuNzM2OTA4IDQuNDQ4NDA4NCwyOS41MzgyNDUgNC40MTI1NTU4LDIwLjMyODE3MiBMIDQuMzc1OTU5MSwxMC45MjY5MzggMy4wMzMzNDQ3LDEwLjg4MDQxOCBDIDEuODIyMTM2OSwxMC44Mzg0NTEgMS42NTUyNTI1LDEwLjgwMDcyMSAxLjMyODI0NDYsMTAuNDk0OTE1IDAuMzQwNzUwMjYsOS41NzE0NDc1IDAuMzQwNzY2NjUsOC40MzUyNDc1IDEuMzI4Mjg3Niw3LjUxMTg2NDQgTCAxLjY5MDgxNjQsNy4xNzI4ODE0IDUuNTY2NzI0NSw3LjEwNTA4NDcgOS40NDI2MzI3LDcuMDM3Mjg4MSA5LjYyMjEwOTQsNi4xNTU5MzIyIEMgMTAuMzIzNjkxLDIuNzEwNjc4OCAxMy42MzIwNTYsMC4yODIyNTYyOCAxNi44NDI0ODksMC44NTU5OTI3NyAxOC45Mzg0MiwxLjIzMDU1NjUgMjAuNjY0Njk2LDIuNTMxMTY3MyAyMS42MDMyODEsNC40NDI4Njg3IDIxLjkwNDY5LDUuMDU2Nzc0IDIyLjE5MDQ1Nyw1Ljg3NjQxMDYgMjIuMjQ3ODE4LDYuMjkxNTI1NCBsIDAuMTAzMDUsMC43NDU3NjI3IDMuMjc2ODg4LDAuMDY3Nzk3IGMgMy4wNDE2NTgsMC4wNjI5MyAzLjMwNDM0LDAuMDg3OTU2IDMuNjU5MzEyLDAuMzQ4NjIxNyAwLjgxNjg0MywwLjU5OTgzMiAxLjA4NjMzOCwxLjc4MjA2MzEgMC41OTEzMDIsMi41OTM5NDcyIC0wLjM1NzkxLDAuNTg2OTk0IC0wLjkyODc3MSwwLjg1NDA0MSAtMS44MjU2NjUsMC44NTQwNDEgaCAtMC43NDg4OTggbCAtMC4wMzc1LDkuMzQ2MDc2IGMgLTAuMDM2OTEsOS4xOTgzNDIgLTAuMDQxOTIsOS4zNTE5OTQgLTAuMzE3MDYyLDkuNzIwNDM1IC0wLjE1Mzc2LDAuMjA1ODk4IC0wLjQ0ODAzNywwLjUwMDE2NCAtMC42NTM5NDksMC42NTM5MjQgLTAuMzY5MTQxLDAuMjc1NjUgLTAuNTE0MjgsMC4yODAwNjcgLTEwLjM2NDUwMywwLjMxNTM4MyAtOS41Mjk4NjM2LDAuMDM0MTcgLTEwLjAxMTc4MjIsMC4wMjQxNiAtMTAuNDYwMzMxNiwtMC4yMTcxNDQgeiBNIDIzLjQ5ODUwMywyOC4zMjYxOTggYyAwLjg1Mjk3NCwtMC40NDEwOSAwLjgxNTA1NiwtMC4wMzQxOSAwLjgxNTA1NiwtOC43NDY1MzcgMCwtNi45NjY1NTggLTAuMDI1ODcsLTcuOTc2NTk0IC0wLjIxMTA3OCwtOC4yNDEwMTYgTCAyMy44OTE0MDIsMTEuMDM3Mjg4IEggMTUuNzY2OTggNy42NDI1NTczIGwgLTAuMjA2ODcxOSwwLjQwMDA0NiBjIC0wLjMwODcwNTcsMC41OTY5NzEgLTAuMzExMjExLDE1LjU0MjQ4MiAtMC4wMDI3MSwxNi4xOTI1OSAwLjQzNzg0MzYsMC45MjI2ODYgMC4xNDYyOTI4LDAuODkxMjggOC4zMTEwNTE2LDAuODk1MjgxIDYuMjcxODUzLDAuMDAzMSA3LjQyMDcxNCwtMC4wMjY0MSA3Ljc1NDQ4MSwtMC4xOTkwMDcgeiBNIDE5LjQyOTIyNiw2Ljg2Nzc5NjYgQyAxOS40MjQzMDQsNi40NzU1MTI0IDE4Ljk1NDEyNSw1LjU2MDYzMDEgMTguNDc5ODI4LDUuMDIwNDM1MyAxNy42Mjc5OTEsNC4wNTAyNDY0IDE2LjE2NjgzLDMuNjI4OTAyOSAxNC44OTE5ODUsMy45ODU4MzczIDEzLjczODU0Miw0LjMwODc4MTYgMTIuNTI4MDg5LDUuNzEwMjQyNyAxMi41MTk5MjcsNi43MzIyMDM0IGwgLTAuMDAzLDAuMzcyODgxMyBoIDMuNDU3NjI3IGMgMy4yMDM3MTcsMCAzLjQ1NzQwOSwtMC4wMTc0MjUgMy40NTQ2NSwtMC4yMzcyODgxIHoiDQogICAgIGlkPSJwYXRoMzg1NCINCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCINCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwtMC43KSIgLz48cGF0aA0KICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZS13aWR0aDowLjEzNTU5MzIyIg0KICAgICBkPSJtIDkuMzk4MzA1MSwxOS45ODY0NDEgdiAtNi45MTUyNTUgaCAxLjQyMzcyODkgMS40MjM3MjkgdiA2LjkxNTI1NSA2LjkxNTI1NCBIIDEwLjgyMjAzNCA5LjM5ODMwNTEgWiINCiAgICAgaWQ9InBhdGgzODU2Ig0KICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIg0KICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LC0wLjcpIiAvPjxwYXRoDQogICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjAuMTM1NTkzMjIiDQogICAgIGQ9Im0gMTQuNDE1MjU0LDE5Ljk4NjQ0MSB2IC02LjkxNTI1NSBoIDEuNDIzNzI5IDEuNDIzNzI5IHYgNi45MTUyNTUgNi45MTUyNTQgaCAtMS40MjM3MjkgLTEuNDIzNzI5IHoiDQogICAgIGlkPSJwYXRoMzg1OCINCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCINCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwtMC43KSIgLz48cGF0aA0KICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZS13aWR0aDowLjEzNTU5MzIyIg0KICAgICBkPSJtIDE5LjI5NjYxLDE5Ljk4NjQ0MSB2IC02LjkxNTI1NSBoIDEuNDkxNTI2IDEuNDkxNTI1IHYgNi45MTUyNTUgNi45MTUyNTQgSCAyMC43ODgxMzYgMTkuMjk2NjEgWiINCiAgICAgaWQ9InBhdGgzODYwIg0KICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIg0KICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LC0wLjcpIiAvPjwvc3ZnPg==";
}

function closeIMG() {
  return "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+DQo8c3ZnDQogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iDQogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIg0KICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIg0KICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyINCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyINCiAgIHhtbDpzcGFjZT0icHJlc2VydmUiDQogICB3aWR0aD0iNTEycHgiDQogICB2aWV3Qm94PSIwIDAgNTEyIDUxMiINCiAgIHZlcnNpb249IjEuMSINCiAgIGlkPSJMYXllcl8xIg0KICAgaGVpZ2h0PSI1MTJweCINCiAgIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiPjxtZXRhZGF0YQ0KICAgICBpZD0ibWV0YWRhdGE2NCI+PHJkZjpSREY+PGNjOldvcmsNCiAgICAgICAgIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZQ0KICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPjxkYzp0aXRsZT48L2RjOnRpdGxlPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZGVmcw0KICAgICBpZD0iZGVmczYyIiAvPjxwYXRoDQogICAgIGlkPSJwYXRoNTciDQogICAgIGQ9Ik03NC45NjYsNDM3LjAxM2MtOTkuOTctOTkuOTctOTkuOTctMjYyLjA2NSwwLTM2Mi4wMzdjMTAwLjAwMi05OS45NywyNjIuMDY2LTk5Ljk3LDM2Mi4wNjcsMCAgYzk5Ljk3MSw5OS45NzEsOTkuOTcxLDI2Mi4wNjcsMCwzNjIuMDM3QzMzNy4wMzIsNTM2Ljk5OCwxNzQuOTY4LDUzNi45OTgsNzQuOTY2LDQzNy4wMTN6IE0zOTEuNzgyLDEyMC4yMjcgIGMtNzUuMDAxLTc0Ljk4NS0xOTYuNTY0LTc0Ljk4NS0yNzEuNTM0LDBjLTc1LjAwMSw3NC45ODUtNzUuMDAxLDE5Ni41NSwwLDI3MS41MzVjNzQuOTcsNzQuOTg2LDE5Ni41MzMsNzQuOTg2LDI3MS41MzQsMCAgQzQ2Ni43NTQsMzE2Ljc3NSw0NjYuNzU0LDE5NS4yMTIsMzkxLjc4MiwxMjAuMjI3eiBNMTg4LjEyNCwzNjkuMTM3bC00NS4yNTEtNDUuMjY2bDY3Ljg3Ni02Ny44NzdsLTY3Ljg3Ni02Ny44NzZsNDUuMjUxLTQ1LjI2NyAgTDI1NiwyMTAuNzQzbDY3Ljg3Ny02Ny44OTJsNDUuMjUsNDUuMjY3bC02Ny44NzYsNjcuODc2bDY3Ljg3Niw2Ny44NzdsLTQ1LjI1LDQ1LjI2NkwyNTYsMzAxLjI0NUwxODguMTI0LDM2OS4xMzd6IiAvPjxwYXRoDQogICAgIGlkPSJwYXRoNjYiDQogICAgIGQ9Ik0gMjQzLjk0OTQxLDUxMS41MTE0MSBDIDIwNC40Nzc0Niw1MDkuNDA5NTQgMTY1LjQ2MzYyLDQ5OC4yODg1OSAxMzEuODM3LDQ3OS41NTM2IDk2LjY2NDk2NCw0NTkuOTU3NTkgNjQuOTAyNTUzLDQzMC41NDE3OCA0Mi43NDEzMjEsMzk3LjA0MDMyIDkuNTMzNjA4NSwzNDYuODM5NzIgLTQuOTUyOTQ0LDI4Ni4zMjc2NSAxLjk4NjU2MjgsMjI2LjgwMjgxIDEwLjUyMTExNSwxNTMuNTk2MTkgNDkuMjA5NjkxLDg4LjgxOTgyNCAxMDkuODQzMzMsNDYuMjE3OTk1IDE0My44MTc4OSwyMi4zNDcxMjEgMTgzLjI3NjUyLDcuNDAwMTE4MyAyMjYuODYyNzYsMS44OTA4MjIxIDIzNi42NjcxLDAuNjUxNTU0ODkgMjY0LjE2NTkzLDAuMTU4MTYwMjYgMjc1LjE1OTE1LDEuMDI0MjY5OCAzNTkuMjk4NzMsNy42NTMyNzc3IDQzMy4xOTE1OSw1My42MzgxNjIgNDc2LjMyMTEyLDEyNi4yMTEwNSBjIDE4LjA2MTI0LDMwLjM5MTE3IDI5LjQ0NDkxLDY0LjYzNzA2IDMzLjkzNTg5LDEwMi4wOTA1OSAxLjMxMTQzLDEwLjkzNjk3IDEuNDgxMyw0MS40OTE5NiAwLjI5MzMxLDUyLjc1ODc4IC01LjY2MzQzLDUzLjcxMTcgLTI2Ljk2NzgxLDEwMi45Mzc4NyAtNjEuODgzMjgsMTQyLjk4ODI5IC03LjAwMTg0LDguMDMxNTkgLTIyLjE5MTQzLDIyLjgxMjU1IC0yOS45NTQxNiwyOS4xNDgzNCAtNDAuMDQ3NzMsMzIuNjg2MTUgLTg2LjAxODEsNTEuOTA3MzYgLTEzNi42OTMyMSw1Ny4xNTQ0MSAtOC44MDYxNywwLjkxMTgxIC0zMC4zODM3MSwxLjU2OTI1IC0zOC4wNzAyNiwxLjE1OTk1IHogbSAzNS42NzIxNCwtNjQuNjkyODggYyAyMS44OTE1MSwtMi44NzU5NCA0MC41NjQxOCwtOC4zOTExOSA1OS42NTMzOSwtMTcuNjE5NTQgMjAuNjU5OTYsLTkuOTg3NjkgMzYuNDg0NDgsLTIxLjE5ODAyIDUyLjUwMTY5LC0zNy4xOTMwMSAzMS42ODExNSwtMzEuNjM3MTkgNTAuNjk5NzQsLTcxLjU1NDQ1IDU1LjQ1MzYxLC0xMTYuMzg5MTMgMS4zNDM2OSwtMTIuNjcyNSAwLjkwNjU3LC0zNS4wOTY0NiAtMC45MTY0OCwtNDcuMDE1NDQgQyA0NDIuODc4MjEsMjA2LjE0IDQzNy4wMjkyOCwxODcuODcwOTIgNDI2Ljk0NjI3LDE2OC4xMDcxOCA0MTIuMDk5MzUsMTM5LjAwNTcyIDM5MC4xNzIxMiwxMTQuNDQ4MjcgMzYyLjY1NjY3LDk2LjEwNTg5NSAzMzUuOTMxNDcsNzguMjkwMzEgMzA2LjgxODAxLDY3Ljg4NDgzMyAyNzQuNTI1NTMsNjQuNjA2ODM0IDI2NC44MzY3Niw2My42MjMzMzIgMjQ0LjgxNDc0LDYzLjc3OTg5NCAyMzQuOTU2NDQsNjQuOTE2MjQ2IDE3Mi43NjI0Miw3Mi4wODUyNjUgMTE5LjczODg0LDEwNy4wODY1NCA4OC41MTc5ODksMTYxLjU4MTQgNjIuOTcyMzE5LDIwNi4xNzA0NSA1Ni44NjQzNiwyNjEuNjI1OTcgNzEuOTkwNjc3LDMxMS42MzY1MyBjIDMuNDMzMzUsMTEuMzUxMzQgNS45NzAxNjYsMTcuNjk2NDUgMTEuODY0OTc3LDI5LjY3NjgyIDE2LjQwNTE1NiwzMy4zNDExMyA0MS45OTQ1NTYsNjEuMDI1OTggNzMuOTM5NjY2LDc5Ljk5NDQgMjQuODEwMjgsMTQuNzMxODggNTIuNjcyMiwyMy42Mzg4MSA4Mi41NTY5LDI2LjM5MTg3IDguNDU0MjUsMC43Nzg4MyAzMC4zNzg1NiwwLjI4NjkxIDM5LjI2OTMzLC0wLjg4MTA5IHoiDQogICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjAuNTk5NTMxNTkiIC8+PHBhdGgNCiAgICAgaWQ9InBhdGg2OCINCiAgICAgZD0ibSAxNjUuODU4NDIsMzQ2LjI1NzUyIC0yMi4zMjk2LC0yMi4zMzU1NSAzMy41NzMyOSwtMzMuNTY5MjggYyAxOC40NjUzMiwtMTguNDYzMTEgMzMuNTczMywtMzMuODM5MDkgMzMuNTczMywtMzQuMTY4ODUgMCwtMC4zMjk3NiAtMTUuMTc0NTIsLTE1Ljc3NTg3IC0zMy43MjExNiwtMzQuMzI0NjkgbCAtMzMuNzIxMTYsLTMzLjcyNTEyIDIyLjQ3NzQ3LC0yMi40ODM0NiAyMi40Nzc0NiwtMjIuNDgzNDYgMzQuMDI1ODksMzQuMDIxOTYgMzQuMDI1ODgsMzQuMDIxOTYgMzMuODc0MDQsLTMzLjg3MDA2IDMzLjg3NDA0LC0zMy44NzAwNiAyMi4zMzA1NiwyMi4zMzA1NiAyMi4zMzA1NiwyMi4zMzA1NyAtMzMuODcwMDQsMzMuODc0MDMgLTMzLjg3MDA1LDMzLjg3NDAyIDMzLjg1NDMsMzMuODU4MjkgMzMuODU0MzEsMzMuODU4MjggLTIyLjE2NTQzLDIyLjM0NzgxIGMgLTEyLjE5MDk5LDEyLjI5MTI5IC0yMi4zMDEsMjIuNDE1NDcgLTIyLjQ2NjY5LDIyLjQ5ODE4IC0wLjE2NTY5LDAuMDgyNyAtMTUuNTQyNzksLTE1LjA5Mjk0IC0zNC4xNzEzMywtMzMuNzIzNjcgbCAtMzMuODcwMDcsLTMzLjg3NDA1IC0zMy44Nzc5OCwzMy44NzQwNyAtMzMuODc3OTksMzMuODc0MDggeiINCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2Utd2lkdGg6MC41OTk1MzE1OSIgLz48L3N2Zz4=";
}

function exitIMG() {
  return "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgLTAuNCAtMC43IDI2IDI5IiBoZWlnaHQ9IjI5cHgiIHZlcnNpb249IjEuMSIgdmlld0JveD0iLTAuNCAtMC43IDI2IDI5IiB3aWR0aD0iMjZweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PGRlZnMvPjxwYXRoIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTEyLjUsMjcuN0M1LjYsMjcuNywwLDIyLjEsMCwxNS4yYzAtNS40LDMuNC0xMCw4LjItMTEuN1Y3ICBjLTMsMS41LTUsNC43LTUsOC4yYzAsNS4xLDQuMiw5LjMsOS4zLDkuM2M1LjEsMCw5LjMtNC4yLDkuMy05LjNjMC0zLjQtMS44LTYuNC00LjYtOFYzLjZDMjEuOCw1LjUsMjUsMTAsMjUsMTUuMiAgQzI1LDIyLjEsMTkuNCwyNy43LDEyLjUsMjcuN3ogTTEyLjgsMTRjLTEuMSwwLTItMC45LTItMlYyYzAtMS4xLDAuOS0yLDItMnMyLDAuOSwyLDJ2MTBDMTQuOCwxMy4xLDEzLjksMTQsMTIuOCwxNHoiIGZpbGw9IiNGOUY5RjkiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==";
}


// ==============================================  Public functions

/**
 * Returns the menu stage.
 * @memberof MTLG.menu#
 */
function getMenuStage() {
  return _menuStage;
}

/**
 * Resizes the menu.
 * @memberof MTLG.menu#
 */
function resize(scale) {
  _menuStage.scaleX = scale;
  _menuStage.scaleY = scale;
}

/**
 * Resizes the menu in responsive mode.
 * @memberof MTLG.menu#
 */
function resizeResponsive(scaleX, scaleY) {
  _menuStage.scaleX = scaleX;
  _menuStage.scaleY = scaleY;
}

/**
 * @function openMenu
 * @description opens the menu
 * @memberof MTLG.menu#
 */
function openMenu() {
  _menuOpen = true;
  _menuContainer.visible = true;
}

/**
 * @function closeMenu
 * @description closes the menu
 * @memberof MTLG.menu#
 *
 */
function closeMenu() {
  _menuOpen = false;
  _menuContainer.visible = false;
  _resetMenuGameButtons();
}

/**
 * @function startMenu
 * @description shows the menu buttons at the edges
 * @memberof MTLG.menu#
 *
 */
function startMenu() {
  // starts the Menu
  _menuButtonContainer.visible = true;
  _resetMenuGameButtons();
}

/**
 *
 * @function subMenu
 * @description adds a callback to a settings menu
 * @memberof MTLG.menu#
 *
 * @param {function} callback callback function if submenu entry is choosen
 */
function subMenu(callback) {

  //https://www.iconfinder.com/icons/1041637/set_setting_icon#size=512
  _addMenuEntryPicCanvas(settingIMG(), function() {
    closeMenu();
    callback();
  }, 32);
  //_addMenuEntryText("Settings", function() {}, 1); // exit to main menu
  _groupMenuEntriesCanvas();
}

/**
 * @function stopMenu
 * @description hides all menu buttons at the edges
 * @memberof MTLG.menu#
 *
 */
function stopMenu() {
  //stops the menu
  closeMenu();
  _menuButtonContainer.visible = false;
}

/**
 * @function addEntryText
 * @description Add a text menu entry and a callback function as handler
 * @memberof MTLG.menu#
 *
 * @param {String} text text of the menu entry
 * @param {function} callback callback function if menu entry is choosen
 *
 */
function addMenuEntryText(text, callback) {
  _addMenuEntryText(text, callback);
}

/**
 * @function addEntryImg
 * @description Adds an image menu entry and a callback function as handler
 * @memberof MTLG.menu#
 *
 * @param {string} path path to the image
 * @param {function} callback callback function if menu entry is choosen
 *
 */
function addMenuEntryImg(path, callback) {
  _addMenuEntryPic(path, callback);
}

function setTimer(timer) {
  timerCount = timer;
}

function setEdgesToClick(edges) {
  edgesToClick = edges;
}

function setRotateAngel(ang) {
  rotateAngle = ang;
}

/**
 * Checks whether the menu is already initialized.
 * @return {boolean} Initialization state.
 * @memberof MTLG.menu#
 */
function isInitialized(){
  return initalized;
}

/*
 * Initialize the menu modul in MTLG.
 * @memberof MTLG.menu#
 *
 * @param {object} options Basic game options to initalize the menu modul.
 */
function init(options) {
  // fill with inital settings
  console.log("Menu modul loaded.");

  _zoll = options.zoll;
  edges = options.edges || 4;
  edgesToClick = options.edgesToClick || 4;
  rotateAngle = options.rotateAngle || 45;
  timerCount = options.timer || 4000;


  _gameButtonTimer = new Timer(_resetMenuGameButtons, timerCount);
  _gameButtonTimer.pause();

  _stage = MTLG.getStage();
  _renderCanvasMenu();
  _addBasicMenuEntries();
  _groupMenuEntriesCanvas();
  stopMenu();

  if(options.overlayMenu) {
    startMenu();
  }

  initalized = true;
}

/*
 * @function info
 * @description info function is called from mtlg to get information about the modul
 * @memberof MTLG.menu#
 *
 * @return {object} returns standard object with name and note of the modul
 */
function info() {
  return {
    name: 'menu',
    note: "MTLG-Modul that adds an overlay menu."
  };
}


MTLG.menu = {
  init,
  open: openMenu,
  close: closeMenu,
  start: startMenu,
  stop: stopMenu,
  // Do not give a developer the choice to add own entries
  //addEntryText: addMenuEntryText,
  //addEntryImg: addMenuEntryImg,
  addSubMenu: subMenu,
  resize,
  getMenuStage,
  isInitialized,
  resizeResponsive
};
MTLG.addModule(init, info);
