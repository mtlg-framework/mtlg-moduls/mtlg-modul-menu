/**
 * @Author: thiemo
 * @Date:   2016-11-30T16:23:30+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-01T11:16:09+01:00
 *
 * tangible modul for mtlg
 */

"use strict";

var MTLG = (function(m) {

  var tangible = (function() {
    var model, view, controller, _screen, o, p;

    function tangible_model(epsilon) {
      this.epsilon = epsilon || 0.15;
      this.touches = [];
      this.tangibles = [];
      this.game_canvas = o.canvas;
      this.game_stage = o.stage;
      this._freetangibles = [];
      this.definedTangibles = o.tangibles;
      this._MSTouchCounter = 0;
      this._touchIds = [];
      this._freetouchIds = [];
    }

    tangible_model.prototype = {
      _getIdTouchIds: function(pointerId) {
        var _ids;
        _ids = this._touchIds.find(function(e) {
          return e.pid === pointerId;
        });
        return _ids.id;
      },

      // Stack for MS Pointer Events
      getTouchIdentifer: function(pointerId) {
        var _ids;

        function ids(id, pid) {
          this.id = id;
          this.pid = pid;
        }

        function findIds(e) {
          return e.pid === 0;
        }

        if (!(_ids = this._touchIds.find(function(e) {
            return e.pid === pointerId;
          }))) {
          if (this._freetouchIds.length !== 0) {
            _ids = this._freetouchIds.pop();
            this._touchIds[_ids.id].pid = pointerId;

          } else {
            _ids = new ids(this._MSTouchCounter, pointerId);
            this._touchIds.push(_ids);

            this._MSTouchCounter = this._MSTouchCounter + 1;
          }
        }
        return _ids.id;
      },

      // Stack for MS Pointer Events
      delTouchIdentifer: function(pointerId) {
        var _ids;
        _ids = this._touchIds.find(function(e) {
          return e.pid === pointerId;
        });
        this._freetouchIds.push(_ids);
        return _ids.id;
      },

      // detect Tangibles
      detectTriangleTangible: function(dists_rd, tid) {
        // calculate angle of 60 degress or PI/3
        // iterate over all distances
        // filter on the right degree
        var i, j = 0; //counter
        var alpha; // degree

        for (i = 0; i < dists_rd.length; i = i + 1) {
          for (j = i + 1; j < dists_rd.length; j = j + 1) {
            alpha = Math.acos((dists_rd[i].a * dists_rd[j].a + dists_rd[i].b * dists_rd[j].b) / (dists_rd[i].d * dists_rd[j].d));
            if (alpha < Math.PI / 3 + this.epsilon && alpha > Math.PI / 3 - this.epsilon) {
              console.log("triangle tangible detected");
              this.touches[tid].isMainTangible = true;
              this.touches[tid].isTangible = true;
              this.touches[dists_rd[i].id].isTangible = true;
              this.touches[dists_rd[j].id].isTangible = true;
              return this.addtangible([tid, dists_rd[i].id, dists_rd[j].id], dists_rd);
            }
          }
        }
        return false;
      },


      detecttangible: function(tid, sc_dist) {
        var i, //counter
          a, b, // distance value of x-axis and y-axis
          dists = [], //array of distances
          dists_rd, // filtered array
          _ar_defTan = this.definedTangibles; // array with settings of tangibles

        var isTriangle; //detected triangle tangible

        function dist(d, id, a, b, x, y) {
          this.d = d; // distance to p
          this.id = id; // touches id of p
          this.a = a; // x dist
          this.b = b; // y dist
          this.x = x; // x of p
          this.y = y; // y of p
        }

        function rmDistances(dists, epsilon, tangible_dist) {
          var i,
            filter = [];

          for (i = 0; i < dists.length; i = i + 1) {
            if (dists[i].d > tangible_dist - epsilon && dists[i].d < tangible_dist + epsilon) { // epsilon should be calculated from screensize
              filter.push(dists[i]);
            }
          }
          return filter;
        }

        //calculate distance
        for (i = 0; i < this.touches.length; i = i + 1) {
          if (!this.touches[i].offDisplay) {
            a = this.touches[tid].x - this.touches[i].x;
            b = this.touches[tid].y - this.touches[i].y;
            if (!this.touches[i].offDisplay && i !== tid) {
              dists.push(new dist(Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)), i, a, b, this.touches[i].x, this.touches[i].y));
            }
          }
        }

        //delete with wrong distance, estimated tangible distance +- epsilon -> 20
        for (i = 0; i < _ar_defTan.length; i = i + 1) {
          dists_rd = rmDistances(dists, 10, sc_dist * _ar_defTan[i]);
          isTriangle = this.detectTriangleTangible(dists_rd, tid);
          // detect triangle tangible
          if (isTriangle) {
            return isTriangle;
          }
        }

        // Error Handling missing if there are more then one option above!
        return false;
      },

      addtangible: function(tangibletouches, dists_rd) {
        var id;

        function tangibleObject(id) {
          this.id = id;
          this.x = 0;
          this.y = 0;
          this.radius = 0;
        }

        tangibleObject.prototype = {
          calcMiddle: function(t) {
            this.y = (t[this.mainTid].y + t[this.secTid].y + t[this.thirdTid].y) / 3;
            this.x = (t[this.mainTid].x + t[this.secTid].x + t[this.thirdTid].x) / 3;
            this.calcRotation(t[this.mainTid].x, t[this.mainTid].y);
          },

          setTouches: function(touches) {
            this.mainTid = touches[0];
            this.secTid = touches[1];
            this.thirdTid = touches[2];
          },

          calcRotation: function(x, y) {
            var add = 90;
            if (x < this.x) {
              add = 270;
            }
            if (this.y !== 0) {
              this.degree = 180 / Math.PI * Math.atan((this.y - y) / (this.x - x)) + add;
            } else {
              if (this.x < x) {
                this.degree = 90;
              } else {
                this.degree = 270;
              }
            }
          },

          calcRadius: function() {
            var x, y;
            x = this.x - this.mainTouchX;
            y = this.y - this.mainTouchY;
            this.radius = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
          },

          isTouch: function(id) {
            return id === this.secTid || id === this.thirdTid;
          }
        }

        // create tangible
        if (this._freetangibles.length) {
          id = this._freetangibles.pop();
        } else {
          id = this.tangibles.length;
        }


        this.tangibles[id] = this.tangibles[id] || new tangibleObject(id);
        this.tangibles[id].offDisplay = false;

        // set Touches
        this.tangibles[id].setTouches(tangibletouches);

        // calculate middle
        this.tangibles[id].calcMiddle(this.touches);

        this.tangibles[id].mainTouchX = this.touches[tangibletouches[0]].x;
        this.tangibles[id].mainTouchY = this.touches[tangibletouches[0]].y;

        this.tangibles[id].calcRadius();

        this.touches[tangibletouches[0]].tangibleID = id;
        this.touches[tangibletouches[1]].tangibleID = id;
        this.touches[tangibletouches[2]].tangibleID = id;
        return this.tangibles[id];
      },

      mvtangible: function(tanID) {
        if (tanID !== -1) {
          this.tangibles[tanID].calcMiddle(this.touches);
        }
      },

      deltangible: function(id, e) {
        var tid = this.touches[id].tangibleID;
        if (tid !== -1) {
          this.tangibles[tid].offDisplay = true;
          if (this._freetangibles.indexOf(tid) === -1) {
            this._freetangibles.push(tid);
          }
        }
        return tid;
      },

      addTouches: function(id, e, pageX, pageY) {
        var touchObject = function() {};
        if (id !== -1) {
          this.touches[id] = this.touches[id] || new touchObject();
          this.touches[id].x = pageX;
          this.touches[id].y = pageY;
          this.touches[id].isTangible = false;
          this.touches[id].tangibleID = -1;
          this.touches[id].event = e;
          this.touches[id].offDisplay = false;
          return this.touches[id];
        }
        return false;
      },

      mvTouches: function(tid, px, py) {
        this.touches[tid].x = px;
        this.touches[tid].y = py;
      },

      getTouches: function() {
        return this.touches;
      },

      delTouches: function(id, e) {
        if (id !== -1) {
          this.touches[id].isTangible = false;
          this.touches[id].tangibleID = -1;
          this.touches[id].offDisplay = true;
        }
      }
    };

    function tangible_view(model) {
      this._model = model;
    }

    tangible_view.prototype = {

      add_overlay: function() {
        var newNode = o.canvas.cloneNode(true);
        newNode.setAttribute('id', 'tangible_overlay');
        o.canvas.parentNode.insertBefore(newNode, o.canvas.nextSibling);
        this._stage = newNode;
        m.resize();
      }
    };

    function Controller(model, view, _screen) {

      // Handler for Chrome and Firefox
      var handleTouches = function(e) {
        var touches, type, i, l, touch, id;

        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        touches = e.changedTouches;
        type = e.type;
        for (i = 0, l = touches.length; i < l; i++) {
          touch = touches[i];
          id = touch.identifier;
          if (touch.target !== view._stage) {
            continue;
          }

          if (type === "touchstart") {
            handleTouchDown(id, e, touch.pageX, touch.pageY);
          } else if (type === "touchmove") {
            handleTouchMove(id, e, touch.pageX, touch.pageY);
          } else if (type === "touchend" || type === "touchcancel") {
            handleTouchUp(id, e);
          }
        }
      };

      var handleTouchMove = function(pid, e, pageX, pageY) {
        var tid,
          eventTmove,
          target,
          id;

        if (o.browser === "IE" || o.browser === "Edge") {
          id = model.getTouchIdentifer(pid);
        } else {
          id = pid;
        }

        if (model.touches[id].isTangible) { // is tangible
          if (!o.Touches) {
            model.game_stage._handlePointerMove(id, e, pageX, pageY);
          }
          model.mvTouches(id, pageX, pageY);

          if (model.touches[id].isTangible) {
            model.mvtangible(model.touches[id].tangibleID);
            tid = model.touches[id].tangibleID;
            eventTmove = e;
            eventTmove.tangible = model.tangibles[tid];
            model.game_stage._handlePointerMove(1000 + eventTmove.tangible.id, eventTmove, eventTmove.tangible.x, eventTmove.tangible.y);
          }
        } else {
          model.game_stage._handlePointerMove(id, e, pageX, pageY);
        }
      };

      var handleTouchDown = function(pid, e, pageX, pageY) {
        var touches,
          gettangible,
          eventTdown,
          upEvent,
          idEvent,
          target,
          id;

        if (o.browser === "IE" || o.browser === "Edge") {
          id = model.getTouchIdentifer(pid);
        } else {
          id = pid;
        }

        touches = model.addTouches(id, e, pageX, pageY);
        gettangible = model.detecttangible(id, _screen.dist);
        if (gettangible) {
          eventTdown = e;
          eventTdown.tangible = gettangible;
          if (!o.Touches) {
            model.game_stage._handlePointerDown(id, e, pageX, pageY);
          } else {
            idEvent = gettangible.secTid;
            upEvent = model.touches[gettangible.secTid].event;
            model.game_stage._handlePointerUp(idEvent, upEvent, true);
            idEvent = gettangible.thirdTid;
            upEvent = model.touches[gettangible.thirdTid].event;
            model.game_stage._handlePointerUp(idEvent, upEvent, true);
          }
          model.game_stage._handlePointerDown(1000 + eventTdown.tangible.id, eventTdown, eventTdown.tangible.x, eventTdown.tangible.y);
        } else {
          model.game_stage._handlePointerDown(id, e, pageX, pageY);
        }

      };

      var handleTouchUp = function(id, e) {
        var tid,
          eventTup,
          deleted,
          target;

        if (o.browser === "Edge" || o.browser === "IE") {
          id = model.delTouchIdentifer(e.pointerId);
        }

        tid = model.touches[id].tangibleID;

        if (tid !== -1) {
          deleted = model.tangibles[model.touches[id].tangibleID].offDisplay;
          model.deltangible(id, e);
          if (!deleted) {
            eventTup = e;
            eventTup.tangible = model.tangibles[model.touches[id].tangibleID];
            model.game_stage._handlePointerUp(1000 + eventTup.tangible.id, eventTup, true);
          }
        }
        model.delTouches(id, e);
        model.game_stage._handlePointerUp(id, e, true);
      };


      var handleMouse = function(e) {
        var type = e.type,
          pageX = e.pageX,
          pageY = e.pageY;

        e.preventDefault();

        if (type === "mousedown") {
          model.game_stage._handlePointerDown(-1, e, pageX, pageY);
        } else if (type === "mouseup") {
          model.game_stage._handlePointerUp(-1, e, true);
        } else if (type === "dblclick") {
          model.game_stage._handleDoubleClick(e);
        }
      }

      // Handler for IE, Edge and UWP-Apps
      var handleTouchesEdgeIE = function(e) {
        var touches, type, i, l, id;

        e.preventDefault();

        id = e.pointerId;
        type = e.type;

        //only uwp app
        if (e.pointerId !== 1) {
          if (type === "touchstart" || type === "pointerdown") {
            handleTouchDown(id, e, e.pageX, e.pageY);
          } else if (type === "touchmove" || type === "pointermove") {
            handleTouchMove(id, e, e.pageX, e.pageY);
          } else if (type === "touchend" || type === "touchcancel" || type === "pointercancel" || type === "pointerup") {
            handleTouchUp(id, e);
          }
        }
      };

      // Pointerevents for Edge and IE
      function pointerEdgeIE() {
        view._stage.addEventListener("pointerdown", handleTouchesEdgeIE, false);
        view._stage.addEventListener("pointermove", handleTouchesEdgeIE, false);
        view._stage.addEventListener("pointerup", handleTouchesEdgeIE, false);
        view._stage.addEventListener("pointercancel", handleTouchesEdgeIE, false);
      }

      // Pointerevents for Firefox and Chrome
      function pointerChromeFirefox() {
        view._stage.addEventListener("touchstart", handleTouches, false);
        view._stage.addEventListener("touchmove", handleTouches, false);
        view._stage.addEventListener("touchend", handleTouches, false);
        view._stage.addEventListener("touchcancel", handleTouches, false);

        view._stage.addEventListener("mousedown", handleMouse, false);
        view._stage.addEventListener("mouseup", handleMouse, false);
        //view._stage.addEventListener("mousemove", handleMouse, false); // impossible to filter?
        view._stage.addEventListener("dblclick", handleMouse, false);
      }

      view.add_overlay();

      if (o.browser === "Chrome" || o.browser === "Firefox") {
        console.log("Firefox or Chrome");
        pointerChromeFirefox();
      }
      if (o.browser === "Edge" || o.browser === "IE") {
        console.log("IE or Edge");
        pointerEdgeIE();
      }
    }

    function Screen(zoll) {
      this.zoll = zoll || 27; // 84 for Surface Hub -- 27 Iliyama -- 12.5 Yoga 12
      this.diag = this.zoll * 2.54;
      this.height = Math.sqrt(81 * Math.pow(this.diag, 2) / 337);
      this.dist = screen.height / this.height;
    }

    function filterTouches(filter) {
      o.Touches = filter;
    }

    function engineDetection() {
      // Chrome 1+
      if (!!window.chrome && !!window.chrome.webstore) return "Chrome";
      // Opera 8.0+
      if (!!window.opr && !!opr.addons || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) return "Opera";
      // Firefox 1.0+
      if (typeof InstallTrigger !== 'undefined') return "Firefox";
      // Safari 3.0+ "[object HTMLElementConstructor]"
      if (/constructor/i.test(window.HTMLElement) || (function(p) {
          return p.toString() === "[object SafariRemoteNotification]";
        })(!window['safari'] || safari.pushNotification)) return "Safari";
      // Internet Explorer 6-11
      if ( /*@cc_on!@*/ false || !!document.documentMode) return "IE";
      // Edge 20+
      if (!( /*@cc_on!@*/ false || !!document.documentMode) && !!window.StyleMedia) return "Edge";
      // Blink engine detection
      if ((isChrome || isOpera) && !!window.CSS) return "Blink";
      return false;
    }

    function init(objects) {
      o = {
        canvas: m.getStage().canvas, // game canvas objects
        stage: m.getStage(), // game stage object
        zoll: objects.zoll, // display size object
        tangibles: objects.tangibles || [7.3, 5.73], // tangible define settings as array
        Touches: objects.FilterTouches || true, // true => filter tangible touches
        browser: engineDetection()
      };

      _screen = new Screen(o.zoll);
      model = new tangible_model();
      view = new tangible_view(model);
      controller = new Controller(model, view, _screen);
    }

    //Register the tangible module with the MTLG gameframe
    m.addModule(init);

    return {
      init: init,
      filterTouches: filterTouches
    }
  })();

  m.tangible = tangible;
  return m;
})(MTLG);
