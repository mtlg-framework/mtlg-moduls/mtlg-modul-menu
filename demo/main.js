/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-05-08T17:58:18+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-06T08:38:44+01:00
 */

var MTLG = function() {
  var stage,
    screenW,
    screenH,
    moduleInit,
    prop, optionen;

  function init() {
    stage = new createjs.Stage('c_1');
    window.addEventListener("resize", _resize_canvas);
    MTLG.resize();

    optionen = optionen || {};
    defaults = {
      zoll: 27,
      menuEdges: 4
    };

    /*
     * Wenn keine Parameter angegeben werden, default Werte benutzen
     */

    for (prop in defaults) {
      if (prop in optionen) {
        continue;
      }
      optionen[prop] = defaults[prop];
    }

    _loadSettings();
  }

  function _loadSettings() {
    if (MTLG.setting_menu) {
      MTLG.setting_menu.init(stage);
      //MTLG.setting_menu.start([12.5, 27, 84], _loadModules);
      _loadModules(27);
    }
  }

  function _loadModules(zoll) {
    var i;
    optionen.zoll = zoll;

    //Initialize modules
    moduleInit = moduleInit || []; //Make sure this works even if no module was registered
    for (i = 0; i < moduleInit.length; i++) {
      try {
        moduleInit[i](optionen);
      } catch (err) {
        console.log("Error calling init function: ");
        console.log(err);
        console.log(moduleInit[i]);
      }
    }
    _lifecycle();
  }

  function _lifecycle() {
    if (MTLG.game) {
      MTLG.game.init(stage);
    }
  }


  function _getScrW() {
    return screenW;
  }

  function _getScrH() {
    return screenW;
  }

  function _getScrZoll() {
    return screenZoll;
  }

  function _getStage() {
    return stage;
  }

  function resizeStage() {

    let i, l;
    let children = MTLG.getStage().children;
    let scaleX = window.innerWidth / 1920;
    let scaleY = window.innerHeight / 1080;
    let scaleFactor = Math.min(scaleX, scaleY);
    //let bgStage = MTLG.getBackgroundStage();
    console.log(scaleFactor);

    for (i = 0, l = children.length; i < l; i++) {
      // children smaller
      children[i].scaleX = children[i].scaleX / _scaleOld * scaleFactor;
      children[i].scaleY = children[i].scaleY / _scaleOld * scaleFactor;
      // put them on the new position
      children[i].x = children[i].x / _scaleOld * scaleFactor;
      children[i].y = children[i].y / _scaleOld * scaleFactor;
    }

    // scale Background
    //bgStage.scaleX = scaleFactor;
    //bgStage.scaleY = scaleFactor;
    //bgStage.update();

    // safe old values
    _scaleOld = scaleFactor;

    // the level could resize itself
    //if (_currentLevel) _currentLevel.resize();
    return scaleFactor;
  }

  function _resize_canvas() {
    var canvas = document.getElementsByTagName('canvas'),
      i;

    screenW = window.innerWidth;
    screenH = window.innerHeight;

    for (i = 0; i < canvas.length; i = i + 1) {
      canvas[i].height = screenH;
      canvas[i].width = screenW;
    }
    resizeStage();
    MTLG.game.resize_game();
  }

  /*
   * Add an initialization callback for a module.
   * This will be called in MTLG.init().
   */
  function addModule(initCallback) {
    moduleInit = moduleInit || [];
    moduleInit.push(initCallback);
  }


  return {
    init: init,
    resize: _resize_canvas,
    getScreenZoll: _getScrZoll,
    addModule: addModule,
    getStage: _getStage
  }
}();
