/**
 * @Author: thiemo
 * @Date:   2017-10-08T18:57:38+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-06T08:25:01+01:00
 */



var MTLG = (function(m){
  var kinnut = (function() {

    //Hilfsfunktionen zur Kapselung nach Revealing Module Pattern
    var erstelleBasis = function(stage, bereiche, loginmethode){
      var loginscreen = new loginBasis(stage, bereiche, loginmethode);
      this.loginscreen = loginscreen;
    }.bind(this)
    var setzeIconProReihe = function(n){
      loginscreen.setzeIconProReihe(n);
    }
    var setzeAnmeldenNacheinander = function(bool){
      loginscreen.setzeAnmeldenNacheinander(true);
    }
    var setzeFarben = function(stand, bestae, warn, schr){
      loginscreen.setzeFarben(stand, bestae, warn, schr);
    }
    var setzeDB = function(pfad, name){
      loginscreen.setzeDB(pfad, name);
    }
    var setzeZufallDB = function(pfad, name){
      loginscreen.setzeZufallDB(pfad, name);
    }
    var init = function(callback){
      loginscreen.init(callback);
    }
    var setzeAusgabeDB = function(uploadPhpPfad, dbname){
      loginscreen.setzeAusgabeDB(uploadPhpPfad, dbname);
    }
    var gibNutzerNachId = function(id){
      return loginscreen.publicgibNutzerNachId(id);
    }
    var alleAngemeldet = function(){
      return loginscreen.gibAlleAngemeldet();
    }
    var gibAnmeldungen = function(){
      return loginscreen.gibAnmeldungen();
    }
    var nutzerAbmelden = function(g){
      loginscreen.nutzerAbmelden(g);
    }
    var setzeKombination = function(anzahl, tiefe){
      loginscreen.setzeKombination(anzahl, tiefe);
    }


    var mockLogin = false; //Remeber the choise for mock logins
    var mockNumber = 2; //Number of players that are generated for mockLogin
    var loginInit = function(options){
      mockLogin = options.mockLogin || true;
      mockNumber = options.mockNumber;
    }
    MTLG.addModule(loginInit);

    //This is the function that handles login
    var erstelleLogIn = function(stage, method){
      if(mockLogin){ //Simulate login
        doMockLogin();
      }else{//Use real login
        //Use the userLogin demo for now
        MTLG.userLogin.main(stage, handleReady);
      }

    }

    /**
     * This function simulates a login.
     * It does so by creating an array of players that can be used
     * without the need of the login process.
     * This function does not use the database, therefore it will not be initialized.
     */
    var doMockLogin = function(){
      var anmeldungen = [{vorname: "Anton",nachname:"A"},
        {vorname: "Berta",nachname:"B"},
        {vorname: "Chris",nachname:"C"},
        {vorname: "Doris",nachname:"D"},
        {vorname: "Erik",nachname:"E"},
        {vorname: "Fiona",nachname:"F"},
        {vorname: "Gert",nachname:"G"},
        {vorname: "Helga",nachname:"H"},
      ]
      var bufferPlayer;
      for(i = 0; i < mockNumber; i++){
        bufferPlayer = anmeldungen[i];
        //{ id: 18, vorname: "Verena", nachname: "Vogel", bild_name: "default_bilder/user_default.png" }
        MTLG.addPlayer({name:bufferPlayer.vorname + " " + bufferPlayer.nachname});
      }
      //Login is over, go to next part of the game
      MTLG.lc.levelFinished();
    }

    /*
     * Callback after login
     */
    var handleReady = function(anmeldungen){
      console.log("Callback from Login, everbody ready!");
      //Register the players
      var bufferPlayer;
      for(i = 0; i < anmeldungen.length; i++){
        bufferPlayer = MTLG.userLogin.gibNutzerNachId(anmeldungen[i]);
        //{ id: 18, vorname: "Verena", nachname: "Vogel", bild_name: "default_bilder/user_default.png" }
        MTLG.addPlayer({name:bufferPlayer.vorname + " " + bufferPlayer.nachname});
      }

      //Login is over, go to next part of the game
      MTLG.lc.levelFinished();

    }

    //Private Funktion
    //Stellt die Basisfunktionalität des Moduls wie zum Beispiel Datenbankverwaltung, Verwaltung der Arbeitsbereiche und der Eingaben zur Verfügung
    var loginBasis = function(stage, bereiche, loginMethode){

      //Konstruktor Variablen
      this.stage = stage;
      this.bereiche = bereiche;
      this.loginMethode = loginMethode;

      //Definieren der Object Methoden
      this.erstelleLogins = erstelleLogins;
      this.aktiviereEingabe = aktiviereEingabe;
      this.leereFeld = leereFeld;
      this.ladeDB = ladeDB;
      this.gibAlleNutzer = gibAlleNutzer;
      this.init = init;
      this.gibNutzerNachId = gibNutzerNachId;
      this.publicgibNutzerNachId = publicgibNutzerNachId;
      this.setzeIconProReihe = setzeIconProReihe;
      this.setzeAnmeldenNacheinander = setzeAnmeldenNacheinander;
      this.setzeDB = setzeDB;
      this.setzeZufallDB = setzeZufallDB;
      this.gibZufallsNutzer = gibZufallsNutzer;
      this.setzeFarben = setzeFarben;
      this.setzeAusgabeDB = setzeAusgabeDB;
      this.einfuegenInDb = einfuegenInDb;
      this.geraeteIdEinfuegenInDb = geraeteIdEinfuegenInDb;
      this.gibAlleAngemeldet = gibAlleAngemeldet;
      this.gibAnmeldungen = gibAnmeldungen;
      this.nutzerAbmelden = nutzerAbmelden;
      this.setzeKombination = setzeKombination;

      //Objekt Variablen
      this.argumente = new Array();
      this.farbArray = new Array();
      this.anmeldenNacheinander = false;
      this.dbName = "";
      this.dbPfad = "";
      this.zufalldbPfad = "";
      this.zufalldbName = "";
      this.ausgabedbDateiname = null;
      this.uploadPhpPfad = null;
      this.zufallIndex = 1;
      this.alleAngemeldet = false;

      this.dbsReady = 0; //Count the dbs that called back with status ready

      //Initiierende Funktion zum Laden der Datenbank und Zeichnen der Bereiche
      function init(callback){
        //save callback
        this.callback = callback || function(res) {console.log(res)}; //Save callback or default function
        this.db = null;
        this.ladeDB(function(err){
          if(err) {
            console.log(err);
          }
          else {
            console.log("Erstelle logins"); //Debug
            this.erstelleLogins();
          }
        },this);
      }


      //Funktion zum Laden der Datenbank
      function ladeDB(next,othis){
        this.next = next;

        //Laden der Nutzerdatenbank
        var xhrDB = new XMLHttpRequest();
        xhrDB.open('GET', this.dbPfad, true);
        xhrDB.responseType = 'arraybuffer';
        var db = null;
        xhrDB.onload = function(e) {
          var uInt8Array = new Uint8Array(xhrDB.response);
          this.db = new SQL.Database(uInt8Array);
          this.dbsReady ++;
          if(this.dbsReady >= 2) {
            this.next();
          }
        }.bind(this);
        xhrDB.send();

        // Zufallsdatenbank laden, wenn LoginMethode "Messe"
        if(this.loginMethode == 5){
          var xhrZufall = new XMLHttpRequest();
          xhrZufall.open('GET', this.zufalldbPfad, true);
          xhrZufall.responseType = 'arraybuffer';
          var zufall = null;
          xhrZufall.onload = function(e) {
            var uInt8Array = new Uint8Array(xhrZufall.response);
            this.zufall = new SQL.Database(uInt8Array);
            this.dbsReady++;
            if(this.dbsReady >= 2) {
              this.next();
            }
          }.bind(this);
          xhrZufall.send();
        } else {
          this.dbsReady ++;
          if(this.dbsReady >= 2) {
            this.next();
          }
        }
      }

      //Funktion zum erstellen der Arbeitsbereiche, laden der Datenbanken und initialisieren des Eventlisteners fuer Eingaben
      function erstelleLogins(){
        //Variablen
        texteingabe = "";
        nutzerArray = new Array();
        eingabeArray = {};
        eingabeArray.aktiviertesFeld = null;
        eingabeArray.aktivierterNutzer = null;
        eingabeArray.enterEingabeAktiv = 1;
        dbName = this.dbName;
        this.numberOfRegistrations = 0; //Anzahl bereits angemeldeter user
        this.numberOfReady = 0; //Anzahl bestätigter user. Wenn alle fertig: callback
        var increaseReady = function(){ //Function to increase ready users
          this.numberOfReady ++;
          if(this.numberOfReady >= bereiche.length){
            if (this.ausgabedbDateiname != null){
              var dateiname = this.ausgabedbDateiname;
              var phpPfad = this.uploadPhpPfad;
            }else{
              var phpPfad = "./db/upload.php";
              var dateiname = "neue_Datenbank.sqlite";
            }
            var bitArray = this.db.export();
            var blob = new Blob([bitArray], {type: "application/x-sqlite3"});
            var	xhrupload = new XMLHttpRequest;
            xhrupload.open('POST', phpPfad + '?name=' + dateiname, true);
            xhrupload.send(blob);
            this.leereAlleBereiche();
            window.removeEventListener("keydown", eingabe);
            this.alleAngemeldet = true;
            this.callback(this.gibAnmeldungen());//callback with the registered users
          }
        };

        //Erstellen und zeichnen der einzelnen Arbeitsbereiche
        for (u=0; u<bereiche.length; u++){
          nutzerArray[bereiche[u].index] = new loginBereich(this.stage, bereiche[u].index, this, bereiche[u].width, bereiche[u].height, bereiche[u].Ox, bereiche[u].Oy, bereiche[u].rotation, this.loginMethode, this.argumente, increaseReady.bind(this));
          if (this.anmeldenNacheinander){
            nutzerArray[u].setzeWarten(true);
          }else{
            nutzerArray[u].setzeWarten(false);
          }
          nutzerArray[u].zeichneBereich();

          //TODO
          nutzerArray[u].firstTime = true;
        }

        //Wenn anmeldenNacheinander gesetzt, dann zeichne ersten Bereich, warte mit nachfolgendem Bereich, bis voriger Bereich vollständig angemeldet
        if (this.anmeldenNacheinander){
          nutzerArray[0].setzeWarten(false);
          nutzerArray[0].neuladen();
          var anDerReihe = 0;
          var checkAnDerReihe = setInterval(function(){
            console.log("Interval 1"); //TODO
            if (nutzerArray[anDerReihe].nutzerAngemeldet()){
              anDerReihe++;
              nutzerArray[anDerReihe].setzeWarten(false);
              nutzerArray[anDerReihe].neuladen();
            }
            if (anDerReihe + 1 == bereiche.length){
              clearInterval(checkAnDerReihe);
            }
          }.bind(this), 50);
        }


        //Eventlistener für Eingabe über externe USB-Geräte (NFC-Scanner, Tastatur, etc.)
        window.addEventListener("keydown", eingabe.bind(this));
        function eingabe(evt){
          //Ueberprüfen ob Eingabe in einem Arbeitsbereich aktiv ist
          if (this.eingabeArray.aktivierterNutzer != null){
            aktivierterNutzer = this.eingabeArray.aktivierterNutzer;
            //Wenn die Eingabe 'Enter' aktiviert ist, wird eine Datenbankabfrage mit dem String in dem aktuell aktiven Eingabefeld gestartet
            //Das Ergebnis der Datenbank-Abfrage wird als aktueller Nutzer des Bereichs gesetzt und der Bereich neu geladen.
            if (evt.key == 'Enter' && eingabeArray.enterEingabeAktiv == 1){
              aussageGeheimnis = this.db.prepare("SELECT * FROM " + this.dbName + " WHERE geheimnis = $geheimnis");
              if (eingabeArray.aktiviertesFeld['feldEingabe'] != null){
                tempgeheimnis = eingabeArray.aktiviertesFeld['feldEingabe'].text;
              }else{
                tempgeheimnis = texteingabe;
              }
              person = aussageGeheimnis.getAsObject({$geheimnis:tempgeheimnis});
              nutzerArray[aktivierterNutzer].setzeNutzer(person);
              nutzerArray[aktivierterNutzer].zeichneBestaetigung();
              texteingabe = "";
              aussageGeheimnis.free();
              eingabeArray.aktivierterNutzer = null;
              eingabeArray.aktiviertesFeld = null;
            //Wenn eine Eingabe über 'Backspace' erfolgt, wird zunächst die Standardfunktion in Chrome verhindert (zurückgehen)
            //Anschließend wird der String im aktuell aktiven Eingabefeld um die letzte Stelle gekürzt
            }else if (evt.keyCode == 8){
              evt.preventDefault();
              if (eingabeArray.aktiviertesFeld['feldEingabe'] != null){
                eingabeArray.aktiviertesFeld['feldEingabe'].text = eingabeArray.aktiviertesFeld['feldEingabe'].text.slice(0,-1);
              }else{
                texteingabe = texteingabe.slice(0,-1);
              }
            }else if(evt.key != 'Enter' && evt.key != 'Shift' && evt.key != 'Tab' && evt.key != "Alt"){
              if (eingabeArray.aktiviertesFeld['feldEingabe'] != null){
                eingabeArray.aktiviertesFeld['feldEingabe'].text += evt.key;
              }else{
                texteingabe += evt.key;
              }
            }
          }
        }
        this.nutzerArray = nutzerArray;
        this.eingabeArray = eingabeArray;
      }

      //Funktion leert alle Bereiche
      this.leereAlleBereiche = function(){
        for (l=0; l<this.bereiche.length; l++){
          this.nutzerArray[l].leereBereich();
          //console.log ("bereich " + l + " geleert");//Debug
        }
      };

      //Funktion gibt IDs aller angemeldeten Nutzer als Array zurück, gemaeß des zu Beginn uebergebenen Arrays
      function gibAnmeldungen(){
        var anmeldungen = new Array;
        for (an=0; an < this.bereiche.length; an++){
          if (nutzerArray[an].aktiverNutzer == null){
            anmeldungen[an] = null;
          }else{
            anmeldungen[an] = nutzerArray[an].aktiverNutzer.id;
          }
        }
        return anmeldungen;
      }

      //Funktion übergibt den Zustand aller Anmeldungen
      function gibAlleAngemeldet(){
        return this.alleAngemeldet;
      }

      //Funktion um einen Nutzer eines Bereichs oder alle Nutzer abzumelden
      function nutzerAbmelden(nutzer){
        if (this.nutzerArray != undefined){
          if(nutzer == null){
            for(w=0; w<this.bereiche.length; w++){
              this.nutzerArray[w].aktiverNutzer = null;
              this.nutzerArray[w].aktiverNutzerBestaetigt = false;
              this.nutzerArray[w].startIcon = 0;
              this.nutzerArray[w].kombinationsEingabe = "";
            }
          }else{
            this.nutzerArray[nutzer].aktiverNutzer = null;
            this.nutzerArray[nutzer].aktiverNutzerBestaetigt = false;
            this.nutzerArray[nutzer].startIcon = 0;
            this.nutzerArray[nutzer].kombinationsEingabe = "";
          }
        }
      }

      //Setzen der AnmeldenNacheinander Variable
      function setzeAnmeldenNacheinander(b){
        this.anmeldenNacheinander = b;
      }

      //Funktion übergibt Array aller Nutzer in Datenbank
      function gibAlleNutzer(){
        alleNutzer = this.db.exec("SELECT id,vorname,nachname,bild_name FROM " + this.dbName + " ORDER BY vorname");
        return alleNutzer[0];
      }

      //Funktion welche außerhalb des Moduls zugreifbar ist und zu einer Nutzer-ID, Vorname, Nachname und Bildpfad als Objekt zurueckgibt
      function publicgibNutzerNachId(id){
        if(id == -1){
          var nutzer = new Array();
          nutzer.vorname = "Kein";
          nutzer.nachname = "Nutzer";
        }else{
          aussageId = this.db.prepare("SELECT id,vorname,nachname,bild_name FROM " + this.dbName + " WHERE id = $id");
          nutzer = aussageId.getAsObject({$id:id});
          aussageId.free();
        }
        return nutzer;
      }

      //Funktion welche zu einer Nutzer-ID, alle Eigenschaten eines Nutzers als Objekt zurueckgibt
      function gibNutzerNachId(id){
        aussageId = this.db.prepare("SELECT * FROM " + this.dbName + " WHERE id = $id");
        nutzer = aussageId.getAsObject({$id:id});
        aussageId.free();
        return nutzer;
      }

      //Funktion liest aus der Zufallsdatenbank einen neuen Nutzer aus und gibt diesen als Objekt zurück
      function gibZufallsNutzer(){
        aussageZufall = this.zufall.prepare("SELECT * FROM " + this.zufalldbName + " WHERE id = $id");
        var position = this.zufallIndex;
        zufallsNutzer = aussageZufall.getAsObject({$id:position});
        aussageZufall.free();
        this.zufallIndex = position + 1;
        return zufallsNutzer;
      }

      //Funktion fügt neuen Nutzer in Datenbank ein, gibt ID zurück
      function einfuegenInDb(vorname, nachname, geheimnis, admin){
        alleNutzer = this.db.exec("SELECT * FROM " + this.dbName + " ORDER BY vorname");
        id = alleNutzer[0].values.length + 1;
        bildpfad = "login_default/user_default.png";
        this.db.run("INSERT INTO " + this.dbName + " VALUES ("+ id +", '" + geheimnis + "', '"+ vorname + "', '"+ nachname + "', '"+ bildpfad + "', '" + admin + "');");
        return id;
      }

      //Funktion fügt neues Gerät in die Datenbank ein, solange dieses Gerät noch nicht erstellt wurde, gibt ID des Geräts zurück
      function geraeteIdEinfuegenInDb(vorname, nachname, geheimnis, admin){
        aussageGeheimnis = this.db.prepare("SELECT * FROM " + dbName + " WHERE geheimnis = $geheimnis");
        var person = aussageGeheimnis.getAsObject({$geheimnis:geheimnis});
        aussageGeheimnis.free();
        if(person.id == undefined){
          var alleNutzer = this.db.exec("SELECT * FROM " + this.dbName + " ORDER BY vorname");
          var id = alleNutzer[0].values.length + 1;
          var bildpfad = "login_default/user_default.png";
          this.db.run("INSERT INTO " + this.dbName + " VALUES ("+ id +", '" + geheimnis + "', '"+ vorname + "', '"+ nachname + "', '"+ bildpfad + "', '" + admin + "');");
          return id;
        }else{
          return person.id;
        }
      }

      //Setzen der Icon pro Reihe im Auswahlverfahren
      function setzeIconProReihe(n){
        this.argumente.iconProReihe = n;
      }

      //Setzen der Variablen fuer die Farben
      function setzeFarben(standard, bestaetigung, warn, schrift){
        this.farbArray.standard = standard;
        this.farbArray.bestaetigung = bestaetigung;
        this.farbArray.warn = warn;
        this.farbArray.schrift = schrift;
        this.argumente.farbArray = this.farbArray;
      }

      //Wenn eine Eingabe aktiviert werden soll, rufen alle Bereiche immer diese Funktion auf, welche dann nach Zustand das eingabeArray für die eingabe-Funktion beschreibt
      function aktiviereEingabe(aktiviertesFeld, aktivierterNutzer, enter, schattenWert){
        if(enter == 0){
          eingabeArray.enterEingabeAktiv = 0;
        }else{
          eingabeArray.enterEingabeAktiv = 1;
        }
        letztesFeld = eingabeArray.aktiviertesFeld;
        if (!(letztesFeld === aktiviertesFeld) && letztesFeld != null){
          letztesFeld['feld'].shadow = new createjs.Shadow("#000000", schattenWert,schattenWert,schattenWert);
          aktiviertesFeld['feld'].shadow = null;
          eingabeArray.aktiviertesFeld = aktiviertesFeld;
          eingabeArray.aktivierterNutzer = aktivierterNutzer;
        }else if (letztesFeld == null){
          aktiviertesFeld['feld'].shadow = null;
          eingabeArray.aktiviertesFeld = aktiviertesFeld;
          eingabeArray.aktivierterNutzer = aktivierterNutzer;
        }else if (letztesFeld === aktiviertesFeld){
          aktiviertesFeld['feld'].shadow = new createjs.Shadow("#000000", schattenWert,schattenWert,schattenWert);
          eingabeArray.aktiviertesFeld = null;
          eingabeArray.aktivierterNutzer = null;
        };
      }

      //Leert aktuelles Eingabefeld
      function leereFeld(stageArrayEintrag){
        stageArrayEintrag['feldEingabe'].text = "";
      }

      //Setzen der Nutzer-Datenbank
      function setzeDB(dbPfad, dbName){
        this.dbPfad = dbPfad;
        this.dbName = dbName;
      }

      //Setzen der Zufallsdatenbank
      function setzeZufallDB(zufalldbPfad, zufalldbName){
        this.zufalldbPfad = zufalldbPfad;
        this.zufalldbName = zufalldbName;
      }

      //Setzen der Ausgabedatenbank
      function setzeAusgabeDB(uploadPhpPfad, ausgabedbDateiname){
        this.uploadPhpPfad = uploadPhpPfad;
        this.ausgabedbDateiname = ausgabedbDateiname;
      }

      //setzen der Kombinationsvariablen
      function setzeKombination(anzahl, tiefe){
        this.argumente.kombinationsAnzahl = anzahl;
        this.argumente.kombinationsTiefe = tiefe;
      }

    }

    //Private Funktion
    //Fuer jeden Arbeitsbereich wird eine Instanz erzeugt, stellt alle anzeigenden Funktionalitäten zur Verfuegung
    var loginBereich = function(stage, nutzer, loginBasis, bereichsBreite, bereichsHoehe, ursprungX, ursprungY, rotation, loginMethode, argumente, localCallback){

      //Objekt-Variablen
      this.stage = stage;
      this.bereichsBreite = bereichsBreite;
      this.bereichsHoehe = bereichsHoehe;
      this.ursprungX = ursprungX;
      this.ursprungY = ursprungY;
      this.argumente = argumente;
      this.anzahlFelder = 4;
      this.nutzer = nutzer;
      this.rotation = rotation;
      this.loginMethode = loginMethode;
      this.alleContainer = new Array();
      this.aktiverNutzer = null;
      this.startIcon = 0;
      this.kombinationsEingabe = "";
      this.adminKombinationsEingabe = "";
      this.warten = false;
      this.aktiverNutzerBestaetigt = false;
      this.schattenWert = bereichsBreite/100;
      this.kombinationKorrekt = false;

      this.localCallback = localCallback;

      //durch Basis optional gesetzte Objekt-Variablen
      if (argumente.iconProReihe != undefined){
        this.iconProReihe = argumente.iconProReihe;
      }else{
        this.iconProReihe = 4;
      }

      if (this.argumente.farbArray != undefined){
        this.standardFarbe = argumente.farbArray.standard;
        this.bestaetigungsFarbe = argumente.farbArray.bestaetigung;
        this.warnFarbe = argumente.farbArray.warn;
        this.schriftFarbe = argumente.farbArray.schrift;
      }else{
        this.standardFarbe = "grey";
        this.bestaetigungsFarbe = "green";
        this.warnFarbe = "red";
        this.schriftFarbe = "black";
      }

      if (this.argumente.kombinationsAnzahl != undefined){
        this.kombinationsAnzahl = this.argumente.kombinationsAnzahl;
        this.kombinationsTiefe = this.argumente.kombinationsTiefe;
        this.KombinationsIconProReihe = Math.ceil(Math.sqrt(this.kombinationsAnzahl));
      }else{
        this.kombinationsAnzahl = 9;
        this.kombinationsTiefe = 3;
        this.KombinationsIconProReihe = Math.ceil(Math.sqrt(this.kombinationsAnzahl));
      }


      //Objekt-Methoden
      this.zeichneAdmin = zeichneAdmin;
      this.leereBereich = leereBereich;
      this.zeichneToken = zeichneToken;
      this.zeichneBereich = zeichneBereich;
      this.zeichneAuswahl = zeichneAuswahl;
      this.ohneLogin = ohneLogin;
      this.setzeNutzer = setzeNutzer;
      this.zeichneAnmeldung = zeichneAnmeldung;
      this.zeichneBestaetigung = zeichneBestaetigung;
      this.zeichneAnonym = zeichneAnonym;
      this.neuladen = neuladen;
      this.zeichneWort = zeichneWort;
      this.zeichneAdminKombination = zeichneAdminKombination;
      this.zeichneWarten = zeichneWarten;
      this.zeichneKombination = zeichneKombination;
      this.setzeWarten = setzeWarten;
      this.nutzerAngemeldet = nutzerAngemeldet;
      this.nochNichtBestaetigt = nochNichtBestaetigt;

      //Durch die Basis wird immer zeichneBereich() aufgerufen, worauf nach Zustand des Arbeitsbereichs entschieden wird, wie der Bereich gezeichnet wird
      //Zuerst wird überprüft ob gewartet werden soll
      //Dann wird kontrolliert ob ein Nutzer für den Bereich gesetzt ist
      //Sollte kein Nutzer gesetzt sein wird der Loginbereich, nach gesetzter LoginMethode, gezeichnet
      function zeichneBereich(){
        if (this.warten){
          this.zeichneWarten();		//Zeichnet Warteanzeige
        }else{
          if (this.aktiverNutzer != null){
            if (this.aktiverNutzer.admin == 1){
              this.zeichneAdmin();			//Zeichnet Fenster zum Anlegen neuer Nutzer
            }else if (loginMethode == 4){		//Fuer Kombinationsverfahren
              this.zeichneKombination();		//Zeichnet Kombination mit angemeldetem Nutzer
            }else{
              this.zeichneAnmeldung();		//Zeichnet Anmeldungsanzeige
            }
          }else{
            switch (this.loginMethode){
              case 1:		//Zeichnet Tokenverfahren
                this.zeichneToken();
                break;

              case 2:		//Zeichnet Auswahlverfahren
                this.zeichneAuswahl();
                break;

              case 3:		//Zeichnet Wortverfahren
                this.zeichneWort();
                break;

              case 4:		//Zeichnet Auswahlanzeige für Kombinationsverfahren
                this.zeichneAuswahl();
                break;

              case 5: 	//Zeichnet Messe-Login
                this.zeichneAnonym();
                break;

              case 6:		//ueberprueft Geraete ID
                this.ohneLogin();
                break;
            }
          }
          this.zeichneAdminKombination();		//Unsichtbares Overlay für geheime Nutzererstellung
        }
      }

      //Funktion nach Geraeteidentifikationsverfahren
      function ohneLogin(){
        new Fingerprint2().get(function(geheimnis, quellen){
          this.aktiverNutzer = new Array();
          this.aktiverNutzer.vorname = "GeräteID";
          this.aktiverNutzer.nachname = "GeräteID";
          var idInDb = loginBasis.geraeteIdEinfuegenInDb(this.aktiverNutzer.vorname, this.aktiverNutzer.nachname, geheimnis, 0);
          this.aktiverNutzer.id = idInDb;
          this.aktiverNutzerBestaetigt = true;

          this.zeichneBestaetigung();
        }.bind(this));
      }

      //Funktion nach Messeidentifikationsverfahren
      function zeichneAnonym(){
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Setzen der Variablen für Objekte auf der Stage
        var bildPfadNeu = "img/login_default/neuer_nutzer.png";
        var bildPfadAlt = "img/login_default/alter_nutzer.png";
        var farbe = this.standardFarbe;
        var userIconBreite = 2*bereichsBreite/6;
        var userIconHoehe = userIconBreite;
        var userIconRadius = userIconBreite/20;
        var userIconNeuerNutzerPosX = bereichsBreite/4 - userIconBreite/2;
        var userIconAlterNutzerPosX = 3*bereichsBreite/4 - userIconBreite/2;
        var userIconPosY = bereichsHoehe/2 - 2*userIconHoehe/3;

        var keinNutzerIconHoehe = 2*bereichsBreite/15;
        var keinNutzerIconBreite = keinNutzerIconHoehe;
        var keinNutzerIconPosX = 12*bereichsBreite/14;
        var keinNutzerIconPosY = 3*bereichsHoehe/4;
        var keinNutzerIconRadius = keinNutzerIconBreite/20;
        var keinNutzerBildHoehe = 8*keinNutzerIconHoehe/10;
        var keinNutzerBildBreite = 8*keinNutzerIconBreite/10;

        var stageArray = new Array();
        var bildBreite = userIconBreite/10*8;
        var bildHoehe = userIconHoehe/10*8;
        var bildPosX = userIconBreite/10;
        var bildPosY = userIconHoehe/10;
        var schriftgroeße = userIconBreite/10;
        var schriftPosY = userIconHoehe/20*19;

        //Erstellen der grafischen Objekte
        //Button neuer Nutzer
        var feldNeuerNutzer = new createjs.Shape();
        feldNeuerNutzer.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
        feldNeuerNutzer.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['feldNeuerNutzer'] = feldNeuerNutzer;

        var userNameNeuerNutzer = new createjs.Text("Neuer Nutzer", "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        userNameNeuerNutzer.textAlign = "center";
        userNameNeuerNutzer.textBaseline = "middle";
        userNameNeuerNutzer.x = userIconBreite/2;
        userNameNeuerNutzer.y = schriftPosY;
        stageArray['userNameNeuerNutzer'] = userNameNeuerNutzer;
        var userContainerNeuerNutzer = new createjs.Container();
        userContainerNeuerNutzer.x = userIconNeuerNutzerPosX;
        userContainerNeuerNutzer.y = userIconPosY;
        userContainerNeuerNutzer.addChild(feldNeuerNutzer, userNameNeuerNutzer);

        var img = new Image();
        img.src = bildPfadNeu;
        var userBildNeuerNutzer;
        img.onload = function(event){
          userBildNeuerNutzer = new createjs.Bitmap(bildPfadNeu);
          userBildNeuerNutzer.x = bildPosX;
          userBildNeuerNutzer.y = bildPosY;
          userBildNeuerNutzer.scaleX = bildBreite/userBildNeuerNutzer.image.width;
          userBildNeuerNutzer.scaleY = bildHoehe/userBildNeuerNutzer.image.height;
          userContainerNeuerNutzer.addChild(userBildNeuerNutzer);
        }
        // Wenn der Button angeklickt wird, wird ein neuer Nutzer aus der Zufallsdatenbank geholt, dieser in die aktuelle Datenbank eingetragen und als angemeldet gesetzt
        stageArray['feldNeuerNutzer'].addEventListener("click", function(){
          var zufallsNutzer = loginBasis.gibZufallsNutzer();
          var idInDb = loginBasis.einfuegenInDb(zufallsNutzer.vorname, zufallsNutzer.nachname, zufallsNutzer.geheimnis, null);
          this.aktiverNutzer = new Array();
          this.aktiverNutzer.id = idInDb;
          this.aktiverNutzer.vorname = zufallsNutzer.vorname;
          this.aktiverNutzer.nachname = zufallsNutzer.nachname;
          this.aktiverNutzer.geheimnis = zufallsNutzer.geheimnis;
          this.aktiverNutzer.bild_name = "login_default/user_default.png";
          this.aktiverNutzer.neu = true;

          this.zeichneBestaetigung();
        }.bind(this));

        //Button alter Nutzer
        var feldAlterNutzer = new createjs.Shape();
        feldAlterNutzer.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
        feldAlterNutzer.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['feldAlterNutzer'] = feldAlterNutzer;

        var userNameAlterNutzer = new createjs.Text("Alter Nutzer", "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        userNameAlterNutzer.textAlign = "center";
        userNameAlterNutzer.textBaseline = "middle";
        userNameAlterNutzer.x = userIconBreite/2;
        userNameAlterNutzer.y = schriftPosY;
        stageArray['userNameAlterNutzer'] = userNameAlterNutzer;
        var userContainerAlterNutzer = new createjs.Container();
        userContainerAlterNutzer.x = userIconAlterNutzerPosX;
        userContainerAlterNutzer.y = userIconPosY;
        userContainerAlterNutzer.addChild(feldAlterNutzer, userNameAlterNutzer);

        var img = new Image();
        img.src = bildPfadAlt;
        var userBildAlterNutzer;
        img.onload = function(event){
          userBildAlterNutzer = new createjs.Bitmap(bildPfadAlt);
          userBildAlterNutzer.x = bildPosX;
          userBildAlterNutzer.y = bildPosY;
          userBildAlterNutzer.scaleX = bildBreite/userBildAlterNutzer.image.width;
          userBildAlterNutzer.scaleY = bildHoehe/userBildAlterNutzer.image.height;
          userContainerAlterNutzer.addChild(userBildAlterNutzer);
        }

        //Wenn alter Nutzer angeklickt wird, wird der Bereich geleert und das Wortverfahren aufgerufen
        stageArray['feldAlterNutzer'].addEventListener("click", function(){
          this.leereBereich();
          this.zeichneWort();
        }.bind(this));

        var keinNutzerFeld = new createjs.Shape();
        keinNutzerFeld.graphics.beginFill(farbe).drawRoundRect(0, 0, keinNutzerIconBreite, keinNutzerIconHoehe, keinNutzerIconRadius);
        keinNutzerFeld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['keinNutzerFeld'] = keinNutzerFeld;

        var keinNutzerContainer = new createjs.Container();
        keinNutzerContainer.x = keinNutzerIconPosX;
        keinNutzerContainer.y = keinNutzerIconPosY;
        keinNutzerContainer.addChild(keinNutzerFeld);
        stageArray['keinNutzerContainer'] = keinNutzerContainer;

        var img = new Image();
        img.src = "img/login_default/kein_nutzer.png";
        var keinNutzerBild;
        img.onload = function(event){
          keinNutzerBild = new createjs.Bitmap("img/login_default/kein_nutzer.png");
          keinNutzerBild.x = keinNutzerIconBreite/10;
          keinNutzerBild.y = keinNutzerIconHoehe/10;
          keinNutzerBild.scaleX = keinNutzerBildBreite/keinNutzerBild.image.width;
          keinNutzerBild.scaleY = keinNutzerBildHoehe/keinNutzerBild.image.height;
          keinNutzerContainer.addChild(keinNutzerBild);
        }

        //Wenn keine Nutzer in dem Bereich angemeldet werden soll wird die NutzerID des Bereichs auf -1 gesetzt und der Bereich neu geladen
        stageArray['keinNutzerContainer'].addEventListener("click", function(){
          this.aktiverNutzer = new Array();
          this.aktiverNutzer.id = -1;
          this.neuladen();
        }.bind(this));

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        var gesamtContainer = new createjs.Container();
        gesamtContainer.addChild(rahmen);
        gesamtContainer.addChild(userContainerNeuerNutzer);
        gesamtContainer.addChild(userContainerAlterNutzer);
        gesamtContainer.addChild(keinNutzerContainer);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion für Kombination im Auswahl mit Kombinationsverfahren
      function zeichneKombination(){
        //Wenn kein Nutzer in dem Bereich angemeldet werden soll, wird keine Authentifikation benoetigt
        if(this.aktiverNutzer.id == -1){
          this.zeichneAnmeldung();
        }else{
          //Rahmen als Bereichsgrenze
          var rahmen = new createjs.Shape();
          rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

          //Setzen der Variablen fuer Elemente
          var userIconBreite = 3*bereichsBreite/10;
          var userIconHoehe = userIconBreite;
          var userIconRadius = userIconBreite/20;
          var userIconPosX = bereichsBreite/5 - userIconBreite/2;
          var userIconPosY = bereichsHoehe/2 - userIconHoehe/2;

          var stageArray = new Array();
          var bildBreite = userIconBreite/10*8;
          var bildHoehe = userIconHoehe/10*8;
          var bildPosX = userIconBreite/10;
          var bildPosY = userIconHoehe/10;
          var schriftgroeße = userIconBreite/10;
          var schriftPosY = userIconHoehe/20*19;

          //Erstellen der grafischen Objekte
          //Angemeldeter Nutzer
          var feld = new createjs.Shape();
          feld.graphics.beginFill(this.standardFarbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
          stageArray['feld'] = feld;
          var name = this.aktiverNutzer.vorname + this.aktiverNutzer.nachname;
          var userName = new createjs.Text(name, "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
          userName.textAlign = "center";
          userName.textBaseline = "middle";
          userName.x = userIconBreite/2;
          userName.y = schriftPosY;
          stageArray['userName'] = userName;
          var userContainer = new createjs.Container();
          userContainer.x = userIconPosX;
          userContainer.y = userIconPosY;
          userContainer.addChild(feld, userName);

          var img = new Image();
          var bildPfad = "img/" + this.aktiverNutzer.bild_name;
          img.src = bildPfad;
          var userBild;
          img.onload = function(event){
            userBild = new createjs.Bitmap(bildPfad);
            userBild.x = bildPosX;
            userBild.y = bildPosY;
            userBild.scaleX = bildBreite/userBild.image.width;
            userBild.scaleY = bildHoehe/userBild.image.height;
            userContainer.addChild(userBild);
          }

          //Variablen für Kombination
          var kombinationsIconBreite = bereichsBreite/((2*this.KombinationsIconProReihe)+1);
          var kombinationsIconHoehe = kombinationsIconBreite;
          var kombinationsIconRadius = kombinationsIconBreite/20;

          var kompbildBreite = kombinationsIconBreite/10*8;
          var kompbildHoehe = kombinationsIconHoehe/10*8;
          var kompbildPosX = kombinationsIconBreite/10;
          var kompbildPosY = kombinationsIconHoehe/10;

          var gesamtContainer = new createjs.Container();
          var kombinationsstelle = this.kombinationsEingabe.length;
          var anzahlReihen = Math.floor(this.kombinationsAnzahl/this.KombinationsIconProReihe);

          //Zeichnen der Kombinationsbutton, abhaengig von der uebergebenen Anzahl
          for (i = 0; i<this.kombinationsAnzahl; i++){
            (function(){
              var farbe = this.standardFarbe;
              var bildPfad = "./img/login_kombination/" + kombinationsstelle + "/" + (i+1) + ".png";

              //Berechnungen für die Postitionen der Button in Abhaengigkeit von Skalierung und Anzahl
              var reihe = Math.floor(i/this.KombinationsIconProReihe);
              var positionInReihe = (i)%this.KombinationsIconProReihe;
              var kombinationsIconPosX = 2*bereichsBreite/5 + kombinationsIconBreite/(this.KombinationsIconProReihe+1) + (positionInReihe*(kombinationsIconBreite+kombinationsIconBreite/(this.KombinationsIconProReihe+1)));
              var kombinationsIconPosY = kombinationsIconHoehe/(this.KombinationsIconProReihe+1) + (reihe*(kombinationsIconHoehe+kombinationsIconHoehe/(this.KombinationsIconProReihe+1)));

              //Erstellen der Button
              stageArray = new Array();
              var feld = new createjs.Shape();
              feld.graphics.beginFill(farbe).drawRoundRect(0, 0, kombinationsIconBreite, kombinationsIconHoehe, kombinationsIconRadius);
              feld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
              stageArray['feld'] = feld;

              var kombinationsContainer = new createjs.Container();
              kombinationsContainer.x = kombinationsIconPosX;
              kombinationsContainer.y = kombinationsIconPosY;
              kombinationsContainer.addChild(feld);

              //Variable fuer den i-ten Button
              var geheimnisEingabe = i+1;

              var img = new Image();
              img.src = bildPfad;
              var kombinationsBild;
              img.onload = function(event){
                kombinationsBild = new createjs.Bitmap(bildPfad);
                kombinationsBild.x = kompbildPosX;
                kombinationsBild.y = kompbildPosY;
                kombinationsBild.scaleX = kompbildBreite/kombinationsBild.image.width;
                kombinationsBild.scaleY = kompbildHoehe/kombinationsBild.image.height;
                kombinationsContainer.addChild(kombinationsBild);
              }.bind(this);
              //Wenn ein Button angeklickt wird, wird zunächst der String der aktuellen Eingabe um die Variable fuer den Button erweitert
              //Anschließend wird überprüft ob die maximale Tiefe der Kombination erreicht ist
              //Wenn ja, wird ueberprueft ob das eingegebene Geheimnis mit dem Geheimnis der Datenbank übereinstimmt, wenn ja wird die Kombination als Korrekt eingetragen, wenn nein wird der Nutzer abgemeldet
              stageArray['feld'].addEventListener("click", function(){
                this.kombinationsEingabe = this.kombinationsEingabe + geheimnisEingabe;
                if(this.kombinationsEingabe.length == this.kombinationsTiefe){
                  if(this.aktiverNutzer.geheimnis == this.kombinationsEingabe){
                    this.kombinationsEingabe="";
                    this.kombinationKorrekt = true;
                    this.leereBereich();

                    this.zeichneBestaetigung();
                  }else{
                    this.aktiverNutzer = {};
                    this.kombinationsEingabe="";
                    this.kombinationKorrekt = false;
                    this.leereBereich();
                    this.zeichneAnmeldung();
                  }
                }else{
                  this.neuladen();
                }

              }.bind(this));
              gesamtContainer.addChild(kombinationsContainer);
            }.bind(this)());
          }
          //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
          gesamtContainer.addChild(rahmen);
          gesamtContainer.addChild(userContainer);
          gesamtContainer.regX = 0;
          gesamtContainer.regY = 0;
          gesamtContainer.x = this.ursprungX;
          gesamtContainer.y = this.ursprungY;
          gesamtContainer.rotation = this.rotation;
          this.stage.addChild(gesamtContainer);
          this.alleContainer.push(gesamtContainer);
        }
      }

      //Funktion zum bestaetigen  der in dem Arbeitsbereich angemeldeten Identitaet
      function zeichneBestaetigung(){
        this.leereBereich(); //Delete current container
        //Setzen der Variablen in Abhaengigkeit ob ein Nutzer in dem Bereich angemeldet ist
        if (this.aktiverNutzer.id == -1){
          var name = "kein Nutzer";
          var bildPfad = "img/login_default/nutzer_fragezeichen.png";
          var farbe = this.standardFarbe;
        }else{
          var name = this.aktiverNutzer.vorname + " " + this.aktiverNutzer.nachname;
          var bildPfad = "img/" + this.aktiverNutzer.bild_name;
          var farbe = this.bestaetigungsFarbe;
        }
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Setzen der Variablen fuer Elemente
        var userIconBreite = 4*bereichsBreite/10;
        var userIconHoehe = userIconBreite;
        var userIconRadius = userIconBreite/20;
        var userIconPosX = bereichsBreite/4 - userIconBreite/2;
        var userIconPosY = bereichsHoehe/2 - userIconHoehe/2;

        var stageArray = new Array();
        var bildBreite = userIconBreite/10*8;
        var bildHoehe = userIconHoehe/10*8;
        var bildPosX = userIconBreite/10;
        var bildPosY = userIconHoehe/10;
        var schriftgroeße = userIconBreite/10;
        var schriftPosY = userIconHoehe/20*19;

        var buttonHoehe = userIconHoehe/3;
        var buttonBreite = buttonHoehe;
        var buttonPosY = userIconHoehe - buttonHoehe;
        var hakenPosX = 2*bereichsBreite/3 - buttonBreite - buttonBreite/4;
        var kreuzPosX = 2*bereichsBreite/3 + buttonBreite/4;
        var fragezeichenPosX = 2*bereichsBreite/3 - buttonBreite;
        var fragezeichenPosY = -userIconHoehe/6;
        var fragezeichenBreite = 2*buttonBreite;
        var fragezeichenHoehe = 2*buttonHoehe;

        //Erstellen der grafischen Objekte
        var feld = new createjs.Shape();
        feld.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
        stageArray['feld'] = feld;

        var userName = new createjs.Text(name, "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        userName.textAlign = "center";
        userName.textBaseline = "middle";
        userName.x = userIconBreite/2;
        userName.y = schriftPosY;
        stageArray['userName'] = userName;
        var userContainer = new createjs.Container();
        userContainer.x = userIconPosX;
        userContainer.y = userIconPosY;
        userContainer.addChild(feld, userName);


        var img = new Image();
        img.src = bildPfad;
        var userBild;
        img.onload = function(event){
          userBild = new createjs.Bitmap(bildPfad);
          userBild.x = bildPosX;
          userBild.y = bildPosY;
          userBild.scaleX = bildBreite/userBild.image.width;
          userBild.scaleY = bildHoehe/userBild.image.height;
          userContainer.addChild(userBild);
        }
        var stageArrayEintrag = stageArray;

        var img = new Image();
        img.src = 'img/login_default/kreuz.png';
        var abbruchKreuz;
        img.onload = function(event){
          abbruchKreuz = new createjs.Bitmap("img/login_default/kreuz.png");
          abbruchKreuz.x = kreuzPosX;
          abbruchKreuz.y = buttonPosY;
          abbruchKreuz.scaleX = buttonBreite/abbruchKreuz.image.width;
          abbruchKreuz.scaleY = buttonHoehe/abbruchKreuz.image.height;
          userContainer.addChild(abbruchKreuz);
          //Um den angemeldeten Nutzer wieder abzumelden
          abbruchKreuz.addEventListener("click", function(){
            this.aktiverNutzer = null;
            this.kombinationsEingabe = "";
            this.kombinationKorrekt = false;
            this.neuladen();
          }.bind(this));
        }.bind(this)


        var img = new Image();
        img.src = 'img/login_default/haken.png';
        var bestaetigungshaken;
        img.onload = function(event){
          bestaetigungshaken = new createjs.Bitmap(event.target);//TODO: Das hier für alle?
          bestaetigungshaken.x = hakenPosX;
          bestaetigungshaken.y = buttonPosY;
          bestaetigungshaken.scaleX = buttonBreite/bestaetigungshaken.image.width;
          bestaetigungshaken.scaleY = buttonHoehe/bestaetigungshaken.image.height;
          userContainer.addChild(bestaetigungshaken);
          //Um den angemeldeten Nutzer zu bestaetigen
          bestaetigungshaken.addEventListener("click", function(){
            this.aktiverNutzerBestaetigt = true;
            this.leereBereich();
            this.zeichneAnmeldung();
            this.localCallback();
          }.bind(this));
        }.bind(this)


        var img = new Image();
        img.src = 'img/login_default/fragezeichen.png';
        var fragezeichen;
        img.onload = function(event){
          fragezeichen = new createjs.Bitmap("img/login_default/fragezeichen.png");
          fragezeichen.x = fragezeichenPosX;
          fragezeichen.y = fragezeichenPosY;
          fragezeichen.scaleX = fragezeichenBreite/fragezeichen.image.width;
          fragezeichen.scaleY = fragezeichenBreite/fragezeichen.image.height;
          userContainer.addChild(fragezeichen);
        }.bind(this)


        var gesamtContainer = new createjs.Container();

        //Wenn der Nutzer das erste mal angelegt wurde, wird zusätzlich das Geheimnis zur erneuten Anmeldung angezeigt
        if (this.aktiverNutzer.neu){
          var schriftgroeßeGeheimnis = bereichsBreite/20;
          var userGeheimnis = new createjs.Text("Geheimnis: " + this.aktiverNutzer.geheimnis, "bold " + schriftgroeßeGeheimnis + "px Calibri", this.schriftFarbe);
          userGeheimnis.textAlign = "center";
          userGeheimnis.textBaseline = "top";
          userGeheimnis.x = fragezeichenPosX + fragezeichenBreite/2;
          userGeheimnis.y = fragezeichenPosY + fragezeichenHoehe;
          userContainer.addChild(userGeheimnis);
        }

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        gesamtContainer.addChild(rahmen);
        gesamtContainer.addChild(userContainer);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion um darzustellen, dass  der Bereich warten muss
      function zeichneWarten(){
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Setzen der Variablen fuer Elemente
        var wartenIconBreite = bereichsBreite/2;
        var wartenIconHoehe = wartenIconBreite;
        var wartenIconRadius = wartenIconBreite/20;
        var wartenIconPosX = bereichsBreite/2 - wartenIconBreite/2;
        var wartenIconPosY = bereichsHoehe/2 - wartenIconHoehe/2;
        var stageArray = new Array();
        var bildBreite = wartenIconBreite/10*8;
        var bildHoehe = wartenIconHoehe/10*8;
        var bildPosX = wartenIconBreite/10;
        var bildPosY = wartenIconHoehe/10;

        //Erstellen der grafischen Elemente
        var feld = new createjs.Shape();
        feld.graphics.beginFill(this.warnFarbe).drawRoundRect(0, 0, wartenIconBreite, wartenIconHoehe, wartenIconRadius);
        stageArray['feld'] = feld;

        var wartenContainer = new createjs.Container();
        wartenContainer.x = wartenIconPosX;
        wartenContainer.y = wartenIconPosY;
        wartenContainer.addChild(feld);

        var img = new Image();
        img.src = "img/login_default/warten.png";
        var wartenBild;
        img.onload = function(event){
          wartenBild = new createjs.Bitmap("img/login_default/warten.png");
          wartenBild.x = bildPosX;
          wartenBild.y = bildPosY;
          wartenBild.scaleX = bildBreite/wartenBild.image.width;
          wartenBild.scaleY = bildHoehe/wartenBild.image.height;
          wartenContainer.addChild(wartenBild);
        }

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        var gesamtContainer = new createjs.Container();
        gesamtContainer.addChild(rahmen);
        gesamtContainer.addChild(wartenContainer);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion um den angemeldeten Nutzer zu visualisieren
      function zeichneAnmeldung(){
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Setzen der Variablen fuer die Elemente
        if (this.aktiverNutzer.id == undefined){
          var name = "? ? ?";
          var bildPfad = "img/login_default/anonym.png";
          var farbe = this.warnFarbe;
        }else if (this.aktiverNutzer.id == -1){
          var name = "kein Nutzer";
          var bildPfad = "img/login_default/nutzer_fragezeichen.png";
          var farbe = this.standardFarbe;
        }else{
          var name = this.aktiverNutzer.vorname + " " + this.aktiverNutzer.nachname;
          var bildPfad = "img/" + this.aktiverNutzer.bild_name;
          var farbe = this.bestaetigungsFarbe;
        }

        var userIconBreite = bereichsBreite/2;
        var userIconHoehe = userIconBreite;
        var userIconRadius = userIconBreite/20;
        var userIconPosX = bereichsBreite/2 - 2*userIconBreite/3;
        var userIconPosY = bereichsHoehe/2 - userIconHoehe/2;

        if(this.aktiverNutzerBestaetigt){
          var userIconPosX = bereichsBreite/2 - userIconBreite/2;
        }else{
          var userIconPosX = bereichsBreite/2 - 2*userIconBreite/3;
        }

        var stageArray = new Array();
        var bildBreite = userIconBreite/10*8;
        var bildHoehe = userIconHoehe/10*8;
        var bildPosX = userIconBreite/10;
        var bildPosY = userIconHoehe/10;
        var schriftgroeße = userIconBreite/10;
        var schriftPosY = userIconHoehe/20*19;
        var buttonHoehe = userIconHoehe/4;
        var buttonBreite = buttonHoehe;
        var buttonPosX = userIconBreite + userIconBreite/6;
        var buttonPosY = userIconHoehe/2 - buttonHoehe/2;

        //Erstellen der grafischen Elemente
        var feld = new createjs.Shape();
        feld.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);

        stageArray['feld'] = feld;

        var userName = new createjs.Text(name, "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        userName.textAlign = "center";
        userName.textBaseline = "middle";
        userName.x = userIconBreite/2;
        userName.y = schriftPosY;
        stageArray['userName'] = userName;
        var userContainer = new createjs.Container();
        userContainer.x = userIconPosX;
        userContainer.y = userIconPosY;
        userContainer.addChild(feld, userName);

        var img = new Image();
        img.src = bildPfad;
        var userBild;
        img.onload = function(event){
          userBild = new createjs.Bitmap(bildPfad);
          userBild.x = bildPosX;
          userBild.y = bildPosY;
          userBild.scaleX = bildBreite/userBild.image.width;
          userBild.scaleY = bildHoehe/userBild.image.height;
          userContainer.addChild(userBild);
        }
        var stageArrayEintrag = stageArray;

        //Eine Abmeldung soll nur moeglich sein, wenn der Nutzer in dem Bereich noch nicht bestaetigt ist
        if(!this.aktiverNutzerBestaetigt){
          var img = new Image();
          img.src = 'img/login_default/kreuz.png';
          var abbruchKreuz;
          img.onload = function(event){
            abbruchKreuz = new createjs.Bitmap("img/login_default/kreuz.png");
            abbruchKreuz.x = buttonPosX;
            abbruchKreuz.y = buttonPosY;
            abbruchKreuz.scaleX = buttonHoehe/abbruchKreuz.image.width;
            abbruchKreuz.scaleY = buttonHoehe/abbruchKreuz.image.height;
            userContainer.addChild(abbruchKreuz);
            abbruchKreuz.addEventListener("click", function(){
              this.aktiverNutzer = null;
              this.kombinationsEingabe = "";
              this.neuladen();
            }.bind(this));
          }.bind(this)
        }

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        var gesamtContainer = new createjs.Container();
        gesamtContainer.addChild(rahmen);
        gesamtContainer.addChild(userContainer);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion welche ein unsichtbares Overlay mit Hitboxen realisiert, um ueber eine Geheimkombination Nutzer in DB einzufuegen
      function zeichneAdminKombination(){

        //Variablen fuer Hitboxen
        var feldHoehe = bereichsHoehe/10;
        var feldBreite = feldHoehe;
        var alpha = 0;

        //Erstellen der Hitboxen
        //Im Bereich links oben
        var gesamtContainer = new createjs.Container();
        var feldlo = new createjs.Shape();
        feldlo.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        var hitlo = new createjs.Shape();
        hitlo.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        feldlo.hitArea = hitlo;
        feldlo.x = 0;
        feldlo.y = 0;
        feldlo.alpha = alpha;
        //Erste Hitbox fuer Kombination
        feldlo.addEventListener("click", function(){
          if(this.adminKombinationsEingabe == ""){
            this.adminKombinationsEingabe = "1";
          }else{
            this.adminKombinationsEingabe = "1";
          }
        }.bind(this));
        gesamtContainer.addChild(feldlo);

        //Im Bereich rechts oben
        var feldro = new createjs.Shape();
        feldro.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        var hitro = new createjs.Shape();
        hitro.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        feldro.hitArea = hitro;
        feldro.x = bereichsBreite - feldBreite;
        feldro.y = 0;
        feldro.alpha = alpha;
        //Letzte Hitbox fuer Kombination
        //Wenn vorige Hitbox korrekt, melde Admin an und lade neu
        feldro.addEventListener("click", function(){
          if(this.adminKombinationsEingabe == "3"){
            this.aktiverNutzer = new Array();
            this.aktiverNutzer.admin = 1;
            this.neuladen();
          }else{
            this.adminKombinationsEingabe = "";
          }
        }.bind(this));
        gesamtContainer.addChild(feldro);

        //Im Bereich rechts unten
        var feldru = new createjs.Shape();
        feldru.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        var hitru = new createjs.Shape();
        hitru.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        feldru.hitArea = hitru;
        feldru.x = bereichsBreite - feldBreite;
        feldru.y = bereichsHoehe - feldHoehe;
        feldru.alpha = alpha;
        //Zweite Hitbox
        feldru.addEventListener("click", function(){
          if(this.adminKombinationsEingabe == "1"){
            this.adminKombinationsEingabe = "2";
          }else{
            this.adminKombinationsEingabe = "";
          }
        }.bind(this));
        gesamtContainer.addChild(feldru);

        //Im Bereich links unten
        var feldlu = new createjs.Shape();
        feldlu.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        var hitlu = new createjs.Shape();
        hitlu.graphics.beginFill("grey").drawRect(0, 0, feldBreite, feldHoehe);
        feldlu.hitArea = hitlu;
        feldlu.x = 0;
        feldlu.y = bereichsHoehe - feldHoehe;
        feldlu.alpha = alpha;
        //Dritte Hitbox
        feldlu.addEventListener("click", function(){
          if(this.adminKombinationsEingabe == "2"){
            this.adminKombinationsEingabe = "3";
          }else{
            this.adminKombinationsEingabe = "";
          }
        }.bind(this));

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        gesamtContainer.addChild(feldlu);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion nach Wortidentifikationsverfahren
      function zeichneWort(){
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Variablen fuer die Elemente
        var name = "? ? ?";
        var bildPfad = "img/login_default/nutzer_fragezeichen.png";
        var farbe = this.standardFarbe;
        var userIconBreite = 2*bereichsBreite/5;
        var userIconHoehe = userIconBreite;
        var userIconRadius = userIconBreite/20;
        var userIconPosX = bereichsBreite/2 - userIconBreite/2;
        var userIconPosY = bereichsHoehe/2 - 2*userIconHoehe/3;

        var keinNutzerIconHoehe = 2*bereichsBreite/15;
        var keinNutzerIconBreite = keinNutzerIconHoehe;
        var keinNutzerIconPosX = 12*bereichsBreite/14;
        var keinNutzerIconPosY = 3*bereichsHoehe/4;
        var keinNutzerIconRadius = keinNutzerIconBreite/20;
        var keinNutzerBildHoehe = 8*keinNutzerIconHoehe/10;
        var keinNutzerBildBreite = 8*keinNutzerIconBreite/10;

        var stageArray = new Array();
        var bildBreite = userIconBreite/10*8;
        var bildHoehe = userIconHoehe/10*8;
        var bildPosX = userIconBreite/10;
        var bildPosY = userIconHoehe/10;
        var schriftgroeße = userIconBreite/10;
        var schriftPosY = userIconHoehe/20*19;
        var textFeldX = userIconPosX;
        var textFeldY = userIconPosY + userIconHoehe;

        //Erstellen der grafischen Elemente
        var feld = new createjs.Shape();
        feld.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
        feld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['feld'] = feld;

        var userName = new createjs.Text(name, "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        userName.textAlign = "center";
        userName.textBaseline = "middle";
        userName.x = userIconBreite/2;
        userName.y = schriftPosY;
        stageArray['userName'] = userName;
        var userContainer = new createjs.Container();
        userContainer.x = userIconPosX;
        userContainer.y = userIconPosY;
        userContainer.addChild(feld, userName);

        var textFeld = new createjs.Shape();
        textFeld.graphics.beginFill(this.standardFarbe).drawRoundRect(0, 0, userIconBreite, schriftgroeße*2, userIconRadius);
        textFeld.x = 0;
        textFeld.y = schriftgroeße;
        stageArray['textFeld'] = textFeld;

        var feldEingabe = new createjs.Text("", "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        feldEingabe.textAlign = "left";
        feldEingabe.textBaseline = "middle";
        feldEingabe.x = schriftgroeße;
        feldEingabe.y = schriftgroeße*2;
        stageArray['feldEingabe'] = feldEingabe;
        var textFeldContainer = new createjs.Container();
        textFeldContainer.x = textFeldX;
        textFeldContainer.y = textFeldY;
        textFeldContainer.addChild(textFeld, feldEingabe);

        var img = new Image();
        img.src = bildPfad;
        var userBild;
        img.onload = function(event){
          userBild = new createjs.Bitmap(bildPfad);
          userBild.x = bildPosX;
          userBild.y = bildPosY;
          userBild.scaleX = bildBreite/userBild.image.width;
          userBild.scaleY = bildHoehe/userBild.image.height;
          userContainer.addChild(userBild);
        }
        var stageArrayEintrag = stageArray;
        //Wenn das Feld angeklickt wird, wird die Eingabe ueber die Basis aktiviert
        stageArray['feld'].addEventListener("click", function(){loginBasis.aktiviereEingabe(stageArrayEintrag, nutzer, 1, this.schattenWert)}.bind(this));

        var keinNutzerFeld = new createjs.Shape();
        keinNutzerFeld.graphics.beginFill(farbe).drawRoundRect(0, 0, keinNutzerIconBreite, keinNutzerIconHoehe, keinNutzerIconRadius);
        keinNutzerFeld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['keinNutzerFeld'] = keinNutzerFeld;

        var keinNutzerContainer = new createjs.Container();
        keinNutzerContainer.x = keinNutzerIconPosX;
        keinNutzerContainer.y = keinNutzerIconPosY;
        keinNutzerContainer.addChild(keinNutzerFeld);
        stageArray['keinNutzerContainer'] = keinNutzerContainer;

        var img = new Image();
        img.src = "img/login_default/kein_nutzer.png";
        var keinNutzerBild;
        img.onload = function(event){
          keinNutzerBild = new createjs.Bitmap("img/login_default/kein_nutzer.png");
          keinNutzerBild.x = keinNutzerIconBreite/10;
          keinNutzerBild.y = keinNutzerIconHoehe/10;
          keinNutzerBild.scaleX = keinNutzerBildBreite/keinNutzerBild.image.width;
          keinNutzerBild.scaleY = keinNutzerBildHoehe/keinNutzerBild.image.height;
          keinNutzerContainer.addChild(keinNutzerBild);
        }

        stageArray['keinNutzerContainer'].addEventListener("click", function(){
          this.aktiverNutzer = new Array();
          this.aktiverNutzer.id = -1;
          this.neuladen();
        }.bind(this));

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        var gesamtContainer = new createjs.Container();
        gesamtContainer.addChild(rahmen);
        gesamtContainer.addChild(userContainer);
        gesamtContainer.addChild(keinNutzerContainer);
        gesamtContainer.addChild(textFeldContainer);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion um grafische Oberflaeche zum anlegen von Nutzer in der Datenbank zu realisieren
      function zeichneAdmin(){
        console.log("Zeichne admin"); //TODO
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Variablen fuer die Elemente
        var stageArray = new Array();
        var nutzer = this.nutzer;
        var schattenWert = this.schattenWert;
        var feldBreite = bereichsBreite/2;
        var feldHoehe = bereichsHoehe/10;
        var feldAbstandX = bereichsBreite/10;
        var feldAbstandY = bereichsHoehe/5;
        var feldRadius = bereichsBreite/150;
        var schriftGroeße = feldHoehe/2;
        var feldursprungX = - feldRadius;
        var hakenBreite = feldHoehe*2;
        var hakenHoehe = hakenBreite;
        var hakenPosX = 3*bereichsBreite/4;
        var hakenPosY = bereichsHoehe/2 - hakenHoehe;

        //Erstellen der grafischen Elemente
        //Ueber eine Schleife werden mehrere Eingabefelder erzeugt
        for (i=0;i < this.anzahlFelder; i++){
          (function(){
          stageArray[i] = new Array();
          var feld = new createjs.Shape();
          feld.graphics.beginFill("grey").drawRoundRect(0, 0, feldBreite, feldHoehe, feldRadius);
          feld.shadow = new createjs.Shadow("#000000", schattenWert, schattenWert, schattenWert);
          feld.x = feldursprungX;
          feld.y = schriftGroeße;
          stageArray[i]['feld'] = feld;
          var feldName = new createjs.Text("", "bold " + schriftGroeße + "px Calibri", this.schriftFarbe);
          feldName.textAlign = "left";
          feldName.textBaseline = "middle";
          feldName.x = 0;
          feldName.y = 0;
          stageArray[i]['feldName'] = feldName;
          var feldEingabe = new createjs.Text("", "bold " + schriftGroeße + "px Calibri", this.schriftFarbe);
          feldEingabe.textAlign = "left";
          feldEingabe.textBaseline = "middle";
          feldEingabe.x = 0;
          feldEingabe.y = feldHoehe;
          stageArray[i]['feldEingabe'] = feldEingabe;
          var feldContainer = new createjs.Container();
          feldContainer.x = feldAbstandX;
          feldContainer.y = feldAbstandY*(i+(1/2));
          feldContainer.addChild(feld, feldName, feldEingabe);
          var img = new Image();
          img.src = 'img/login_default/kreuz.png';
          var feldKreuz;
          img.onload = function(event){
            feldKreuz = new createjs.Bitmap("img/login_default/kreuz.png");
            feldKreuz.x = feldBreite;
            feldKreuz.y = schriftGroeße;
            feldKreuz.scaleX = feldHoehe/feldKreuz.image.width;
            feldKreuz.scaleY = feldHoehe/feldKreuz.image.height;
            feldContainer.addChild(feldKreuz);
            feldKreuz.addEventListener("click", function(){loginBasis.leereFeld(stageArrayEintrag)});
          }
          stageArray[i]['feldKreuz'] = feldKreuz;
          stageArray[i]['feldContainer'] = feldContainer;
          var feldIndex = i;
          var stageArrayEintrag = stageArray[i];
          stageArray[i]['feldIndex'] = feldIndex;
          stageArray[i]['feld'].addEventListener("click", function(){loginBasis.aktiviereEingabe(stageArrayEintrag, nutzer, 0, schattenWert)}.bind(this));
          }());
        }

        //Ueberschriften der jeweiligen Felder
        stageArray[0]['feldName'].text = "Vorname";
        stageArray[1]['feldName'].text = "Nachname";
        stageArray[2]['feldName'].text = "Geheimnis";
        stageArray[3]['feldName'].text = "Admin";

        //Button um Nutzer in Datenbank eintragen
        var gesamtContainer = new createjs.Container();
        var imghaken = new Image();
        imghaken.src = 'img/login_default/haken.png';
        var haken;
        imghaken.onload = function(event){
          haken = new createjs.Bitmap("img/login_default/haken.png");
          haken.x = hakenPosX;
          haken.y = hakenPosY;
          haken.scaleX = hakenHoehe/haken.image.width;
          haken.scaleY = hakenBreite/haken.image.height;
          gesamtContainer.addChild(haken);
          haken.addEventListener("click", function(){
            loginBasis.einfuegenInDb(stageArray[0]['feldEingabe'].text, stageArray[1]['feldEingabe'].text, stageArray[2]['feldEingabe'].text, stageArray[3]['feldEingabe'].text);
            this.aktiverNutzer = null;
            this.neuladen();
          }.bind(this));
        }.bind(this)

        //Button um Eingabe abzubrechen
        var imgkreuz = new Image();
        imgkreuz.src = 'img/login_default/kreuz.png';
        var kreuz;
        imgkreuz.onload = function(event){
          kreuz = new createjs.Bitmap("img/login_default/kreuz.png");
          kreuz.x = hakenPosX;
          kreuz.y = hakenPosY + hakenHoehe;
          kreuz.scaleX = hakenHoehe/kreuz.image.width;
          kreuz.scaleY = hakenBreite/kreuz.image.height;
          gesamtContainer.addChild(kreuz);
          kreuz.addEventListener("click", function(){
            this.aktiverNutzer = null;
            this.neuladen();
          }.bind(this));
        }.bind(this)

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        gesamtContainer.addChild(rahmen);
        for (i=0; i<this.anzahlFelder; i++){
          gesamtContainer.addChild(stageArray[i]['feldContainer']);
        }
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion nach Tokenidentifikationsverfahren
      function zeichneToken(){

        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Variablen fuer die Elemente
        var name = "? ? ?";
        var bildPfad = "img/login_default/nutzer_fragezeichen.png";
        var farbe = this.standardFarbe;
        var userIconBreite = bereichsBreite/2;
        var userIconHoehe = userIconBreite;
        var userIconRadius = userIconBreite/20;
        var userIconPosX = bereichsBreite/2 - userIconBreite/2;
        var userIconPosY = bereichsHoehe/2 - userIconHoehe/2;

        var keinNutzerIconHoehe = 2*bereichsBreite/15;
        var keinNutzerIconBreite = keinNutzerIconHoehe;
        var keinNutzerIconPosX = 12*bereichsBreite/14;
        var keinNutzerIconPosY = 3*bereichsHoehe/4;
        var keinNutzerIconRadius = keinNutzerIconBreite/20;
        var keinNutzerBildHoehe = 8*keinNutzerIconHoehe/10;
        var keinNutzerBildBreite = 8*keinNutzerIconBreite/10;

        var stageArray = new Array();
        var bildBreite = userIconBreite/10*8;
        var bildHoehe = userIconHoehe/10*8;
        var bildPosX = userIconBreite/10;
        var bildPosY = userIconHoehe/10;
        var schriftgroeße = userIconBreite/10;
        var schriftPosY = userIconHoehe/20*19;

        //Erstellen der grafischen Elemente
        var feld = new createjs.Shape();
        feld.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
        feld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['feld'] = feld;

        var userName = new createjs.Text(name, "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
        userName.textAlign = "center";
        userName.textBaseline = "middle";
        userName.x = userIconBreite/2;
        userName.y = schriftPosY;
        stageArray['userName'] = userName;
        var userContainer = new createjs.Container();
        userContainer.x = userIconPosX;
        userContainer.y = userIconPosY;
        userContainer.addChild(feld, userName);

        var img = new Image();
        img.src = bildPfad;
        var userBild;
        img.onload = function(event){
          userBild = new createjs.Bitmap(bildPfad);
          userBild.x = bildPosX;
          userBild.y = bildPosY;
          userBild.scaleX = bildBreite/userBild.image.width;
          userBild.scaleY = bildHoehe/userBild.image.height;
          userContainer.addChild(userBild);
        }
        var stageArrayEintrag = stageArray;
        //Aktivieren der Eingabe Mithilfe der Basis
        stageArray['feld'].addEventListener("click", function(){loginBasis.aktiviereEingabe(stageArrayEintrag, nutzer, null, this.schattenWert)}.bind(this));

        var keinNutzerFeld = new createjs.Shape();
        keinNutzerFeld.graphics.beginFill(farbe).drawRoundRect(0, 0, keinNutzerIconBreite, keinNutzerIconHoehe, keinNutzerIconRadius);
        keinNutzerFeld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
        stageArray['keinNutzerFeld'] = keinNutzerFeld;

        var keinNutzerContainer = new createjs.Container();
        keinNutzerContainer.x = keinNutzerIconPosX;
        keinNutzerContainer.y = keinNutzerIconPosY;
        keinNutzerContainer.addChild(keinNutzerFeld);
        stageArray['keinNutzerContainer'] = keinNutzerContainer;

        var img = new Image();
        img.src = "img/login_default/kein_nutzer.png";
        var keinNutzerBild;
        img.onload = function(event){
          keinNutzerBild = new createjs.Bitmap("img/login_default/kein_nutzer.png");
          keinNutzerBild.x = keinNutzerIconBreite/10;
          keinNutzerBild.y = keinNutzerIconHoehe/10;
          keinNutzerBild.scaleX = keinNutzerBildBreite/keinNutzerBild.image.width;
          keinNutzerBild.scaleY = keinNutzerBildHoehe/keinNutzerBild.image.height;
          keinNutzerContainer.addChild(keinNutzerBild);
        }

        //Funktion um keinen Nutzer in dem Bereich anzumelden
        stageArray['keinNutzerContainer'].addEventListener("click", function(){
          this.aktiverNutzer = new Array();
          this.aktiverNutzer.id = -1;
          this.neuladen();
        }.bind(this));

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        var gesamtContainer = new createjs.Container();
        gesamtContainer.addChild(rahmen);
        gesamtContainer.addChild(userContainer);
        gesamtContainer.addChild(keinNutzerContainer);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion nach Auswahlidentifikationsverfahren
      function zeichneAuswahl(){
        //Rahmen als Bereichsgrenze
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginStroke("#000").setStrokeStyle(1).drawRect(0,0,this.bereichsBreite, this.bereichsHoehe);

        //Variablen fuer die Elemente
        var iconProReihe = this.iconProReihe;
        var alleNutzer = loginBasis.gibAlleNutzer();
        var userIconBreite = bereichsBreite/(iconProReihe+2);
        var userIconHoehe = userIconBreite;
        var userIconRadius = userIconBreite/20;

        var bildBreite = userIconBreite/10*8;
        var bildHoehe = userIconHoehe/10*8;
        var bildPosX = userIconBreite/10;
        var bildPosY = userIconHoehe/10;
        var schriftgroeße = userIconBreite/10;
        var schriftPosY = userIconHoehe/20*19;
        var pfeilHoehe = bereichsHoehe/4;
        var pfeilBreite = userIconBreite;
        var pfeilPosX = userIconBreite/(iconProReihe+1) + (iconProReihe*(userIconBreite+userIconBreite/(iconProReihe+1))) - 1; //-1 fuer Pixel offset des Rahmens
        var pfeilHochPosY = bereichsHoehe/2 - 3*pfeilHoehe/2;
        var pfeilRunterPosY = bereichsHoehe/2 + pfeilHoehe/2;

        var gesamtContainer = new createjs.Container();

        //Erstellen der grafischen Elemente
        //Pfeil zum Scrollen der Auswahl - runter
        var imgRunter = new Image();
        imgRunter.src = 'img/login_default/pfeilRunter.png';
        var pfeilRunter;
        imgRunter.onload = function(event){
          pfeilRunter = new createjs.Bitmap("img/login_default/pfeilRunter.png");
          pfeilRunter.x = pfeilPosX;
          pfeilRunter.y = pfeilRunterPosY;
          pfeilRunter.scaleX = pfeilBreite/pfeilRunter.image.width;
          pfeilRunter.scaleY = pfeilHoehe/pfeilRunter.image.height;
          //Pfeil soll ausgegraut werden, wenn nicht nutzbar
          if(this.startIcon + iconProReihe > alleNutzer.values.length - 1){
            pfeilRunter.alpha = 0.2;
          }else{
            pfeilRunter.addEventListener("click", function(){
              this.startIcon = this.startIcon + iconProReihe;
              this.neuladen();
            }.bind(this));
          }
          gesamtContainer.addChild(pfeilRunter);
        }.bind(this);

        //Pfeil zum Scrollen der Auswahl - hoch
        var imgHoch = new Image();
        imgHoch.src = 'img/login_default/pfeilHoch.png';
        var pfeilHoch;
        imgHoch.onload = function(event){
          pfeilHoch = new createjs.Bitmap("img/login_default/pfeilHoch.png");
          pfeilHoch.x = pfeilPosX;
          pfeilHoch.y = pfeilHochPosY;
          pfeilHoch.scaleX = pfeilBreite/pfeilHoch.image.width;
          pfeilHoch.scaleY = pfeilHoehe/pfeilHoch.image.height;
          //Pfeil soll ausgegraut werden, wenn nicht nutzbar
          if(this.startIcon < iconProReihe){
            this.startIcon = 0;
            pfeilHoch.alpha = 0.2;
          }else{
            pfeilHoch.addEventListener("click", function(){
              this.startIcon = this.startIcon - iconProReihe;
              this.neuladen();
            }.bind(this));
          }
          gesamtContainer.addChild(pfeilHoch);
        }.bind(this);

        //In das Nutzerarray wird vorne ein Nutzer zum Abmelden des Bereichs eingefuegt
        var keinNutzer = new Array();
        keinNutzer[0] = -1;
        keinNutzer[1] = "kein";
        keinNutzer[2] = "Nutzer";
        keinNutzer[3] = "login_default/kein_nutzer.png";
        alleNutzer.values.unshift(keinNutzer);

        //Erstellen der grafischen Matrix aller Nutzer im Array
        for (i=this.startIcon; i<alleNutzer.values.length; i++){
          (function(){

            //Setzen der Variablen fuer einen Button
            var name = alleNutzer.values[i][1] + " " + alleNutzer.values[i][2];
            var nutzerID = alleNutzer.values[i][0];
            var farbe = this.standardFarbe;

            if (alleNutzer.values[i][3] == null){
              var bildPfad = "img/login_default/user_default.png";
            }else{
              var bildPfad = "img/" + alleNutzer.values[i][3];
            }

            var reihe = Math.floor((i-this.startIcon)/iconProReihe);
            var positionInReihe = (i-this.startIcon)%iconProReihe;
            var userIconPosX = userIconBreite/(iconProReihe+1) + (positionInReihe*(userIconBreite+userIconBreite/(iconProReihe+1)));
            var userIconPosY = userIconHoehe/(iconProReihe+1) + (reihe*(userIconHoehe+userIconHoehe/(iconProReihe+1)));

            //Erstellen der grafischen Elemente
            stageArray = new Array();
            var feld = new createjs.Shape();
            feld.graphics.beginFill(farbe).drawRoundRect(0, 0, userIconBreite, userIconHoehe, userIconRadius);
            feld.shadow = new createjs.Shadow("#000000", this.schattenWert, this.schattenWert, this.schattenWert);
            stageArray['feld'] = feld;

            var userName = new createjs.Text(name, "bold " + schriftgroeße + "px Calibri", this.schriftFarbe);
            userName.textAlign = "center";
            userName.textBaseline = "middle";
            userName.x = userIconBreite/2;
            userName.y = schriftPosY;
            stageArray['userName'] = userName;

            var userContainer = new createjs.Container();
            userContainer.x = userIconPosX;
            userContainer.y = userIconPosY;
            userContainer.addChild(feld, userName);

            var img = new Image();
            img.src = bildPfad;
            var userBild;
            img.onload = function(event){
              userBild = new createjs.Bitmap(bildPfad);
              userBild.x = bildPosX;
              userBild.y = bildPosY;
              userBild.scaleX = bildBreite/userBild.image.width;
              userBild.scaleY = bildHoehe/userBild.image.height;
              userContainer.addChild(userBild);
            }.bind(this);
            stageArray['feld'].addEventListener("click", function(){
              if(nutzerID == -1){
                this.aktiverNutzer = new Array();
                this.aktiverNutzer.id = -1;
              }else{
                this.aktiverNutzer = loginBasis.gibNutzerNachId(nutzerID);
                if(this.loginMethode == 4) {
                  this.neuladen();
                } else if(this.loginMethode == 2) {
                  this.zeichneBestaetigung();
                }
              }
            }.bind(this));

            //Wenn der Button sich innerhalb des Arbeitsbereichs befindet, wird er zum Arbeitsbereich hinzugefuegt
            if(userIconPosY < this.bereichsHoehe){
              gesamtContainer.addChild(userContainer);
            }
          }.bind(this)());
        }

        //Alle Elemente werden zu einem Contrainer hinzugefügt, welcher auf die Koordinaten und Rotation des Bereichs gesetzt und zur Stage hinzugefuegt wird
        gesamtContainer.addChild(rahmen);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = this.ursprungX;
        gesamtContainer.y = this.ursprungY;
        gesamtContainer.rotation = this.rotation;
        this.stage.addChild(gesamtContainer);
        this.alleContainer.push(gesamtContainer);
      }

      //Funktion zum leeren des Arbeitsbereichs
      function leereBereich(){
        for(i=0; i<this.alleContainer.length; i++){
          this.stage.removeChild(this.alleContainer[i]);
        }
        this.alleContainer = new Array();
      }

      //Funktion zum erneuten Laden des Arbeitsbereichs
      function neuladen(){
        this.leereBereich();
        this.zeichneBereich();
      }

      //Setzen eines Nutzers
      function setzeNutzer(person){
        this.aktiverNutzer = person;
      }

      //Setzen der warten Variable
      function setzeWarten(b){
        this.warten = b;
      }

      //Ueberprueft ob ein Nutzer in dem Arbeitsbereich angemeldet ist in Abhaenigigkeit von dem Verfahren
      function nutzerAngemeldet(){
        if (this.aktiverNutzer != null){
            if (this.aktiverNutzer.id == undefined){
              return false;
            }else if (this.aktiverNutzer.admin == 1){
              return false;
            }else if (this.loginMethode == 4 && this.kombinationKorrekt == false && this.aktiverNutzer.id != -1){
              return false;
            }else{
              return true;
            }
        }else{
          return false;
        }
      }

      //Ueberprueft ob der Nutzer in dem Bereich bestaetigt worden ist
      function nochNichtBestaetigt(){
        if (this.aktiverNutzerBestaetigt){
          return false;
        }else{
          return true;
        }
      }

    }

    function main(stage,callback){
      // Gegeben durch MTLG Framework
      var fensterBreite = m.getOptions().width; //ctx.width;
      var fensterHoehe = m.getOptions().height; //ctx.height;

      var buttonHoehe = fensterHoehe/12;
      var buttonBreite = fensterBreite/5;
      var schriftGroeße = fensterHoehe/30;
      var radius = buttonHoehe/10;
      var startPosX = fensterBreite/17;
      var startPosY = fensterHoehe/5;
      var feldAbstandY = buttonHoehe/2;
      var feldAbstandX = buttonBreite/10;

      var verfahrenArray = new Array();
      var optionenArray = new Array();
      var layoutArray = new Array();
      var anmeldungenArray = new Array();

      var verfahren = null;
      var layout = null;
      var iconProReihe = false;
      var anmeldenNacheinander = false;
      var farben = false;
      var kombination = false;

      menu();

      function menu(anmeldungen){
        //Rahmen
        var rahmen = new createjs.Shape();
        rahmen.graphics.beginFill("#e8f1fa").beginStroke("#000").setStrokeStyle(1).drawRect(0,0,fensterBreite, fensterHoehe);

        var verfahrenName = new createjs.Text("Verfahren", "bold " + 3*schriftGroeße/2 + "px Calibri", "black");
        verfahrenName.textAlign = "center";
        verfahrenName.textBaseline = "middle";
        verfahrenName.x = startPosX + buttonBreite/2;
        verfahrenName.y = startPosY/2;
        //Auswahl-Button fuer Verfahren
        for (i=0; i<6; i++){
          (function(){
            switch(i){
              case 0:
                var verfahrenid = 3;
                break;

              case 1:
                var verfahrenid = 2;
                break;

              case 2:
                var verfahrenid = 4;
                break;

              case 3:
                var verfahrenid = 1;
                break;

              case 4:
                var verfahrenid = 5;
                break;

              case 5:
                var verfahrenid = 6;
                break;
            }

            verfahrenArray[i] = new Array();
            var feld = new createjs.Shape();
            feld.graphics.beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
            verfahrenArray[i]['feld'] = feld;
            var feldName = new createjs.Text("", "bold " + schriftGroeße + "px Calibri", "black");
            feldName.textAlign = "center";
            feldName.textBaseline = "middle";
            feldName.x = buttonBreite/2;
            feldName.y = buttonHoehe/2;
            verfahrenArray[i]['feldName'] = feldName;
            var feldContainer = new createjs.Container();
            feldContainer.x = startPosX;
            feldContainer.y = startPosY + i*(buttonHoehe + feldAbstandY);
            feldContainer.addChild(feld, feldName);
            verfahrenArray[i]['container'] = feldContainer;
            verfahrenArray[i]['feld'].addEventListener("click", function(){
              if (verfahren == verfahrenid){
                feld.graphics.clear().beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                verfahren = null;
              }else if(verfahren == null){
                feld.graphics.clear().beginFill("green").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                verfahren = verfahrenid;
              }
            }.bind(this));
          }());
        }
        verfahrenArray[0]['feldName'].text = "Wortverfahren";
        verfahrenArray[1]['feldName'].text = "Auswahlverfahren";
        verfahrenArray[2]['feldName'].text = "Auswahl und Komb.";
        verfahrenArray[3]['feldName'].text = "Tokenverfahren";
        verfahrenArray[4]['feldName'].text = "Messeverfahren";
        verfahrenArray[5]['feldName'].text = "Geräteverfahren";


        var layoutName = new createjs.Text("Layout", "bold " + 3*schriftGroeße/2 + "px Calibri", "black");
        layoutName.textAlign = "center";
        layoutName.textBaseline = "middle";
        layoutName.x = startPosX + 3*buttonBreite/2 + feldAbstandX;
        layoutName.y = startPosY/2;
        //Layout-Button
        for (i=0; i<5; i++){
          (function(){
            var layoutid = i;
            layoutArray[i] = new Array();
            var feld = new createjs.Shape();
            feld.graphics.beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
            layoutArray[i]['feld'] = feld;
            var feldName = new createjs.Text("", "bold " + schriftGroeße + "px Calibri", "black");
            feldName.textAlign = "center";
            feldName.textBaseline = "middle";
            feldName.x = buttonBreite/2;
            feldName.y = buttonHoehe/2;
            layoutArray[i]['feldName'] = feldName;
            var feldContainer = new createjs.Container();
            feldContainer.x = startPosX + buttonBreite + feldAbstandX;
            feldContainer.y = startPosY + i*(buttonHoehe + feldAbstandY);
            feldContainer.addChild(feld, feldName);
            layoutArray[i]['container'] = feldContainer;
            layoutArray[i]['feld'].addEventListener("click", function(){
              if (layout == layoutid){
                feld.graphics.clear().beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                layout = null;
              }else if(layout == null){
                feld.graphics.clear().beginFill("green").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                layout = layoutid;
              }
            }.bind(this));
          }());
        }

        layoutArray[0]['feldName'].text = "Ein Nutzer";
        layoutArray[1]['feldName'].text = "Zwei Nutzer";
        layoutArray[2]['feldName'].text = "Vier Nutzer";
        layoutArray[3]['feldName'].text = "Vier Nutzer - rotiert";
        layoutArray[4]['feldName'].text = "Acht Nutzer";

        var optionenName = new createjs.Text("Optionen", "bold " + 3*schriftGroeße/2 + "px Calibri", "black");
        optionenName.textAlign = "center";
        optionenName.textBaseline = "middle";
        optionenName.x = startPosX + 2*feldAbstandX + 5*buttonBreite/2;
        optionenName.y = startPosY/2;
        //Optionen-Button
        for (i=0; i<6; i++){
          (function(){
            var index = i;
            optionenArray[i] = new Array();
            var feld = new createjs.Shape();
            feld.graphics.beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
            optionenArray[i]['feld'] = feld;
            var feldName = new createjs.Text("", "bold " + schriftGroeße + "px Calibri", "black");
            feldName.textAlign = "center";
            feldName.textBaseline = "middle";
            feldName.x = buttonBreite/2;
            feldName.y = buttonHoehe/2;
            optionenArray[i]['feldName'] = feldName;
            var feldContainer = new createjs.Container();
            feldContainer.x = startPosX + 2*feldAbstandX + 2*buttonBreite;
            feldContainer.y = startPosY + i*(buttonHoehe + feldAbstandY);
            feldContainer.addChild(feld, feldName);
            optionenArray[i]['container'] = feldContainer;
            optionenArray[i]['feld'].addEventListener("click", function(){
              switch(index){
                case 0:
                  if(anmeldenNacheinander){
                    feld.graphics.clear().beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    anmeldenNacheinander = false;
                  }else{
                    feld.graphics.clear().beginFill("green").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    anmeldenNacheinander = true;
                  }
                  break;

                case 1:
                  if(iconProReihe){
                    feld.graphics.clear().beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    iconProReihe = false;
                  }else{
                    feld.graphics.clear().beginFill("green").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    iconProReihe = true;
                  }
                  break;

                case 2:
                  if(kombination){
                    feld.graphics.clear().beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    kombination = false;
                  }else{
                    feld.graphics.clear().beginFill("green").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    kombination = true;
                  }
                  break;

                case 3:
                  if(farben){
                    feld.graphics.clear().beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    farben = false;
                  }else{
                    feld.graphics.clear().beginFill("green").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
                    farben = true;
                  }
                  break;
              }
            }.bind(this));
          }());
        }
        optionenArray[0]['feldName'].text = "Anmelden in Rotation";
        optionenArray[1]['feldName'].text = "Icon pro Reihe ändern";
        optionenArray[2]['feldName'].text = "Kombination ändern";
        optionenArray[3]['feldName'].text = "Farben ändern";

        //Start-Button
        var startArray = new Array();
        var startfeld = new createjs.Shape();
        startfeld.graphics.beginFill("white").beginStroke("black").drawRoundRect(0, 0, buttonBreite/2, buttonHoehe*2, radius);
        startArray['startfeld'] = startfeld;
        var startfeldName = new createjs.Text("Start", "bold " + schriftGroeße + "px Calibri", "black");
        startfeldName.textAlign = "center";
        startfeldName.textBaseline = "middle";
        startfeldName.x = buttonBreite/4;
        startfeldName.y = buttonHoehe;
        startArray['startfeldName'] = startfeldName;
        var startfeldContainer = new createjs.Container();
        startfeldContainer.x = startPosX + 15*buttonBreite/4;
        startfeldContainer.y = 3*fensterHoehe/4;
        startfeldContainer.addChild(startfeld, startfeldName);
        startArray['container'] = startfeldContainer;
        startArray['startfeld'].addEventListener("click", function(){
          var bereiche;
          switch(layout){
            case 0: bereiche = MTLG.createAreas(1); break;
            case 1: bereiche = MTLG.createAreas(2); break;
            case 2: bereiche = MTLG.createAreas(4); break;
            case 4: bereiche = MTLG.createAreas(8); break;
            default: console.log("Currently unsupported layout: " + layout);
          }
          stage.removeChild(gesamtContainer);
          erstelleBasis(stage, bereiche, verfahren);
          if(iconProReihe){
            setzeIconProReihe(3);
          }
          if(anmeldenNacheinander){
            setzeAnmeldenNacheinander(true);
          }
          if(farben){
            setzeFarben("black","blue","purple","green");
          }
          if(kombination){
            setzeKombination(4,2);
          }
          setzeDB("./db/test.sqlite", "test");
          setzeZufallDB("./db/zufall.sqlite", "zufall");
          setzeAusgabeDB("./db/upload.php", "test.sqlite");
          init(callback);

        }.bind(this));

        if(anmeldungen	!= null){
          console.log(anmeldungen);
          var anmeldungenName = new createjs.Text("Anmeldungen", "bold " + 3*schriftGroeße/2 + "px Calibri", "black");
          anmeldungenName.textAlign = "center";
          anmeldungenName.textBaseline = "middle";
          anmeldungenName.x = startPosX + 5*feldAbstandX + 7*buttonBreite/2;
          anmeldungenName.y = startPosY/2;
          //Auswahl-Button fuer Verfahren
          for (i=0; i<anmeldungen.length; i++){
            (function(){
              anmeldungenArray[i] = new Array();
              var feld = new createjs.Shape();
              feld.graphics.beginFill("#8EBAE5").beginStroke("black").drawRoundRect(0, 0, buttonBreite, buttonHoehe, radius);
              anmeldungenArray[i]['feld'] = feld;
              var nutzer = gibNutzerNachId(anmeldungen[i]);
              console.log(nutzer);
              var pos = i+1;
              var name = ""+ pos +". " + nutzer.vorname + " " + nutzer.nachname;
              var feldName = new createjs.Text(name, "bold " + schriftGroeße + "px Calibri", "black");
              feldName.textAlign = "left";
              feldName.textBaseline = "middle";
              feldName.x = feldAbstandX;
              feldName.y = buttonHoehe/2;
              anmeldungenArray[i]['feldName'] = feldName;
              var feldContainer = new createjs.Container();
              feldContainer.x = startPosX + 6*buttonBreite/2 + 5*feldAbstandX;
              feldContainer.y = startPosY + i*(buttonHoehe + feldAbstandY);
              feldContainer.addChild(feld, feldName);
              anmeldungenArray[i]['container'] = feldContainer;
            }());
          }
        }

        //
        var gesamtContainer = new createjs.Container();
        gesamtContainer.addChild(rahmen);
        for(i=0; i<6; i++){
          gesamtContainer.addChild(verfahrenArray[i]['container']);
        }
        for(i=0; i<4; i++){
          gesamtContainer.addChild(optionenArray[i]['container']);
        }
        for(i=0; i<5; i++){
          gesamtContainer.addChild(layoutArray[i]['container']);
        }
        if(anmeldungen != null){
          for(i=0; i<anmeldungen.length; i++){
            gesamtContainer.addChild(anmeldungenArray[i]['container']);
          }
        }
        gesamtContainer.addChild(anmeldungenName);
        gesamtContainer.addChild(optionenName);
        gesamtContainer.addChild(verfahrenName);
        gesamtContainer.addChild(layoutName);
        gesamtContainer.addChild(startArray['container']);
        gesamtContainer.regX = 0;
        gesamtContainer.regY = 0;
        gesamtContainer.x = 0;
        gesamtContainer.y = 0;
        stage.addChild(gesamtContainer);
        if(anmeldungen != null){
          MTLG.zumHauptmenue(anmeldungen);
        }
      }

    }


    //diese Funktionen existieren außerhalb des Moduls
    return {
      erstelleBasis: erstelleBasis,
      setzeIconProReihe: setzeIconProReihe,
      setzeAnmeldenNacheinander: setzeAnmeldenNacheinander,
      setzeFarben: setzeFarben,
      setzeDB: setzeDB,
      setzeZufallDB: setzeZufallDB,
      setzeAusgabeDB: setzeAusgabeDB,
      start: init,
      gibNutzerNachId: gibNutzerNachId,
      alleAngemeldet: alleAngemeldet,
      nutzerAbmelden: nutzerAbmelden,
      setzeKombination: setzeKombination,
      gibAnmeldungen: gibAnmeldungen,
      main: main,
      erstelleLoginScreen: erstelleLogIn,
    }
  })();

  //Add userLogin to MTLG
  m.userLogin = kinnut;
  //Add the function erstelleLoginScreen
  m.erstelleLoginScreen = kinnut.erstelleLoginScreen;
  return m;
})(MTLG);
