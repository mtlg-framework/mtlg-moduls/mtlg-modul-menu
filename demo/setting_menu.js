/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-05-08T10:41:07+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-08-08T09:14:09+02:00
 */

// ToDo Make it as Plugin -> General easy settingpage -> Later perhaps as Menu

var MTLG = (function(m) {

  var setting_menu = (function() {
    var stage, b1, screenW, _screenH, callback;

    _screenW = window.innerWidth;
    _screenH = window.innerHeight;

    function button(text, place) {
      this.c = new createjs.Container();
      this.b = new createjs.Shape();
      this.l = new createjs.Text(text, "bold 24px Arial", "#FFFFFF");

      this.b.graphics.beginFill("green").drawRoundRect(0, 0, 150, 60, 10);

      this.l.textAlign = "center";
      this.l.textBaseline = "middle";
      this.l.x = 150 / 2;
      this.l.y = 60 / 2;

      this.c.regX = 75;
      this.c.regY = 30;
      this.c.text = this.l.text;

      this.setPos(place);
      this.c.addChild(this.b, this.l);
      this.c.on("click", handleButtonClick);

      stage.addChild(this.c);

      function handleButtonClick(e) {
        stage.removeAllChildren();
        stage.update();
        callback(text);
      }
    }

    button.prototype = {
      setPos: function(place) {
        var bsPerRow = (_screenW - _screenW % 180) / 180;

        this.c.x = place % bsPerRow * 180 + 80;
        this.c.y = ((place - place % bsPerRow / bsPerRow)) * 80 + 50;
      }
    }

    function init(gamestage) {
      stage = gamestage;
    }

    function start(settings, _callback) {
      var i = 0;
      callback = _callback;

      settings.forEach(function(el) {
        var b = new button(el, i);
        i = i + 1;
      });
      stage.update();
    }

    return {
      init: init,
      start: start
    }
  })();

  m.setting_menu = setting_menu;
  return m;
})(MTLG);
