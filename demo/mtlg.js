/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-08-17T19:26:38+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-06T08:16:07+01:00
 */

/**
 * MTLG framework
 *
 * @namespace MTLG
 *
 **/
var MTLG = (function() {


  /*
   * program variables (initialization further below)
   */
  var stage,
    stageContainer,
    data,
    intlData,
    options,
    settings,
    modulsMTLG,
    ticker,
    levelIndex,
    players,
    moduleInit,
    moduleInfo,
    blockingModules,
    deprecationWarningState,
    gameInit,
    backgroundStage,
    scaleFactor;

  /*
   * print deprecation warning once
   */
  var deprecation_warning = function(oldFunc, newFunc) {
    if (deprecationWarningState[oldFunc] === undefined) {
      console.warn(`Deprecated use: '${oldFunc}', use '${newFunc}' instead`);
      deprecationWarningState[oldFunc] = true;
    }
  }

  /**
   * @function init -
   * This function initializes the learning game.
   * It is called when the onload-event of body in index.html is fired.
   * The function loads all settings, creates a stage, initializes modules
   * and calls the game init function.
   * @memberof! MTLG
   */
  var mtlg_init = function() {

    deprecationWarningState = {};

    /*
     * Default-Options
     * language is set as follows:
     * 1. are languages defined in the browser?
     * 2. if yes, take the top language - end
     * 3. if no, take the user preferred language - end
     */
    options = options || {};
    defaults = {
      get hoehe() {
        deprecation_warning("options.hoehe", "options.height");
        return this.height;
      },
      set hoehe(v) {
        deprecation_warning("options.hoehe", "options.height");
        this.height = v;
      },
      get breite() {
        deprecation_warning("options.breite", "options.width");
        return this.width;
      },
      set breite(v) {
        deprecation_warning("options.breite", "options.width");
        this.width = v;
      },
      width: screen.width,
      height: screen.height,
      dd: {
        host: "http://kildin.informatik.rwth-aachen.de",
        port: 3125
      },
      zoll: 27,
      countdown: 180,
      fps: 60,
      playernumber: 4,
      mockLogin: false, //Skip login and use dummies instead
      mockNumber: 4 //Number of dummies created
    };

    /*
     * use default values when no parameter are given
     */

    options = Object.assign(defaults, options);


    /*
     * Set the language for the game:
     * First try to find language setting (set in device_manifest)
     * Next try to set the browser prefered language as game language.
     * Choose the first available language else.
     */
    /*if (!options.language || !MTLG.lang.setLanguage(options.language)) {
      console.log("Device manifest language could not be set: " + options.language);
      var browserLang = navigator.languages ? navigator.languages[0] : navigator.language;
      browserLang = browserLang.split("-")[0];
      if (!MTLG.lang.setLanguage(browserLang)) {
        console.log("Browser language " + browserLang + " not supported!");
        MTLG.lang.setLanguage(options.languages[0]);
      }
    }*/

    //adapt canvasObject to the requirements from options:
    var canvas = document.getElementById('canvasObject');

    //assign canvasObject
    stage = new createjs.Stage(canvas);
    stageContainer = new createjs.Container();
    stageContainer.setBounds(stage.getBounds());
    stage.addChild(stageContainer);

    //enable touch
    createjs.Touch.enable(stage);
    // deprecate global stage object
    Object.defineProperty(window, 'stage', {
      get: function() {
        deprecation_warning("global stage", "MTLG.getStage")
        return window.__stage
      },
      set: function(v) {
        deprecation_warning("global stage", "MTLG.getStage")
        window.__stage = v
      }
    });

    // add background canvas
    backgroundStage = new createjs.Stage(_addBackgroundCanvas());

    // scale stage to window size
    resize_canvas();

    players = [];

    //Initialize modules
    moduleInit = moduleInit || []; //Make sure this works even if no module was registered
    moduleInfo = moduleInfo || []; //Make sure this works even if no module was registered

    blockingModules = {}; //Some modules have to be present before the game starts
    for (var i = 0; i < moduleInit.length; i++) {
      if (modulsMTLG && modulsMTLG.hasOwnProperty(moduleInfo[i]().name) && modulsMTLG[moduleInfo[i]().name] === 0) { // moduleInfo[i]().name === 'tabulaEvents'
        console.log('Modul ' + moduleInfo[i]().name + ' not loaded, because it is blocked in manifest/framework.settings.js.');
      } else {
        try {
          var retValue = (moduleInit[i])(options, moduleCallback);
          if (retValue) { //Blocking modules return their handle
            blockingModules[retValue] = true;
          }
        } catch (err) {
          console.log("Error calling init function: ");
          console.log(err);
          console.log(moduleInit[i]);
        }
      }
    }

    //Initialize game
    try {
      gameInit(options);
    } catch (err) {
      console.log("Error calling game init function: ");
      console.log(err);
      console.log(gameInit);
    }

    // there are no blocking modules
    if(Object.keys(blockingModules).length == 0)
      allModulesDone();
  }

  function _addBackgroundCanvas() {
    let newNode = stage.canvas.cloneNode(true);
    newNode.setAttribute('id', 'background');
    stage.canvas.parentNode.insertBefore(newNode, stage.canvas);
    return newNode;
  }

  /**
   * This function is callec by blocking modules once they are done.
   * If all blocking modules are finished, this function calls allModulesDone.
   * @param moduleHandle the handle, as specified in the return value of the module init function
   */
  function moduleCallback(moduleHandle) {
    if (blockingModules[moduleHandle]) {
      blockingModules[moduleHandle] = false;
    } else {
      console.log("Callback from unknown module: " + moduleHandle);
    }
    var modulesLeft = false;
    for (var property in blockingModules) {
      if (blockingModules.hasOwnProperty(property)) {
        if (blockingModules[property]) {
          modulesLeft = true;
          break;
        }
      }
    }
    if (!modulesLeft) {
      allModulesDone();
    }
  }

  /**
   * This function is called after all modules have been initialized.
   * It starts the game and stage update ticker.
   */
  function allModulesDone() {
    console.log("All modules loaded");
    // start game
    createjs.Ticker.setFPS(options.fps);
    createjs.Ticker.addEventListener('tick', stage);
    MTLG.lc.startGame(); //lc takes care of login etc.

  };

  /**
   * @function resize - resize all canvas overlays, keeping aspect ratio
   * @memberof! MTLG
   */
  function resize_canvas() {
    var canvas = document.getElementsByTagName('canvas'),
      i;
    for (i = 0; i < canvas.length; i = i + 1) {
      canvas[i].height = window.innerHeight;
      canvas[i].width = window.innerWidth;
    }

    scaleFactor = MTLG.lc.resizeStage();
  }

  /**
   * @function fixedSizeCm -
   * makes size of displayObjects absolute
   * @param obj - the object that will be scaled to the absolute size (should have bounds)
   * @param wCm - width in centimeters
   * @param hCm - height in centimeters
   * @param refObj - default: parent. Scaling of all ancestors up to this point is adopted, everything futher up is NOT scaled
   * @memberof! MTLG
   */
  function fixedSizeCm(obj, wCm, hCm, refObj = null) {
    // check input
    if (obj.getBounds() == undefined)
      return console.warn("object has no bounds, can't set fixed size [you can define bounds with setBounds()]");
    obj._scaleX = obj.scaleX;
    obj._scaleY = obj.scaleY;
    var dpi = Math.sqrt(screen.width ** 2 + screen.height ** 2) / options.zoll;
    Object.defineProperty(obj, 'scaleX', {
      get: function() {
        for (var i = refObj || obj.parent, s = 1; i != null; i = i.parent) s *= i.scaleX;
        return this._scaleX / s * (wCm / (obj.getBounds().width / dpi * 2.54));
      }
    });
    Object.defineProperty(obj, 'scaleX', {
      set: function(v) {
        this._scaleX = v;
      }
    });
    Object.defineProperty(obj, 'scaleY', {
      get: function() {
        for (var i = refObj || obj.parent, s = 1; i != null; i = i.parent) s *= i.scaleY;
        return this._scaleY / s * (hCm / (obj.getBounds().height / dpi * 2.54));
      }
    });
    Object.defineProperty(obj, 'scaleY', {
      set: function(v) {
        this._scaleY = v;
      }
    });
  }

  /**
   * @function addModule -
   * Add an initialization callback for a module.
   * This will be called in MTLG.init().
   * @param {function} initCallback - the init function of the new module
   * @param {function} info - a function that returns version, name and type information of the module
   * @memberof! MTLG
   */
  function addModule(initCallback, info) {
    moduleInit = moduleInit || [];
    moduleInit.push(initCallback);
    moduleInfo = moduleInfo || [];
    info = info || function() {
      return {
        version: 'No version added.',
        name: 'No name added.',
        type: 'No type added.'
      }
    };
    moduleInfo.push(info);
  }

  /**
   * @function addGameInit -
   * This function is used to register the game init function with the MTLG framework.
   * It should be called in the game js file.
   * @param {function} initCallback - init function of game
   * @memberof! MTLG
   */
  function addGameInit(initCallback) {
    gameInit = initCallback;
  }



  //////////////////////////////////////


  /*
   * This function is called at end of a level and starts the next
   */
  var geheZumNaechstenAbschnitt = function() {
    //If the lc is used call levelFinished (without a gameState!)
    if (MTLG.lc.getLevels().length >= 1) {
      MTLG.lc.levelFinished();
    } else { //Old way
      switch (levelIndex) {
        case 0:
          levelIndex++;
          Game.login_init();
          break;
        case 1:
          levelIndex++;
          Game.spielfeld_init();
          break;
        case 2:
          levelIndex++;
          Game.feedback_init();
          drawFeedbackOptions();
          break;
        default:
          console.log('No valid input in current field!');
      }
    }

  };


  ////////////////////////


  /*
   * This function draws the feedbackoptions
   * Since there is only one area for feedback, no parameter is needed
   */
  var drawFeedbackOptions = function() {
    var hit = new createjs.Shape();
    hit.graphics.beginFill('black').drawRect(0, 0, 300, 150);

    var button = new createjs.Bitmap('./img/menueButton.png');
    button.x = 10;
    button.y = options.height - 160;
    button.image.onload = function() {
      stage.update();
    };
    button.hitArea = hit;
    button.addEventListener('click', function(event) {
      levelIndex = 1;
      geheZumNaechstenAbschnitt();
    });

    var text = '{{NewGame}}'
    var template = Handlebars.compile(text);
    var buttonText = new createjs.Text(template(data), '30px Arial', 'yellow');
    buttonText.x = (160 - (buttonText.getMeasuredWidth() / 2));
    buttonText.y = button.y + ((150 - buttonText.getMeasuredHeight()) / 2);

    var hit2 = new createjs.Shape();
    hit2.graphics.beginFill('black').drawRect(0, 0, 300, 150);

    var hmbutton = new createjs.Bitmap('./img/menueButton.png');
    hmbutton.x = button.x + 300 + 25;
    hmbutton.y = button.y;
    hmbutton.image.onload = function() {
      stage.update();
    };
    hmbutton.hitArea = hit2;
    hmbutton.addEventListener('click', function(event) {
      MTLG.lc.goToMenu();
    });

    text = '{{Menutext_ToMenu}}';
    template = Handlebars.compile(text);
    var hmText = new createjs.Text(template(data), '30px Arial', 'yellow');
    hmText.x = (hmbutton.x + 150 - (hmText.getMeasuredWidth() / 2));
    hmText.y = (hmbutton.y + ((150 - hmText.getMeasuredHeight()) / 2));

    stage.addChild(button);
    stage.addChild(buttonText);
    stage.addChild(hmbutton);
    stage.addChild(hmText);
    stage.update();
  };


  /////////////////////////////////

  /**
   * @function getStage -
   * This function returns the stage object
   * @memberof! MTLG
   */
  var getStage = function() {
    return stage;
  };

  /**
   * @function clearStage -
   * This function clears the stage by removing all children
   * @memberof! MTLG
   */
  var clearStage = function() {
    stage.removeAllChildren();
    stageContainer.removeAllChildren();
    stage.addChild(stageContainer.set({scaleX:1,scaleY:1}));
  };

  /**
   * @function loadOptions -
   * Load new options - overwrites old options where the same key is used
   * @param {object} newOptions - an object containing the new options
   * @memberof! MTLG
   */
  var loadOptions = function(newOptions) {
    options = options || {};
    //Override local settings if existing
    for (var prop in newOptions) {
      options[prop] = newOptions[prop];
    }
  };

  /**
   * @function loadSettings -
   * Load new settings - overwrites old settings where the same key is used
   * @param {object} newSettings - an object containing the new settings
   * @memberof! MTLG
   */
  var loadSettings = function(newSettings) {
    settings = settings || {
      default: {},
      all: {}
    };
    //Override local settings if existing
    for (var prop in newSettings) {
      for (var prop2 in newSettings[prop]) {
        settings[prop][prop2] = newSettings[prop][prop2];
      }
    }
  };

  /**
   * @function loadModulsMTLG - overwrites old module options
   * @param {object} newOptions - the new module options
   * @memberof! MTLG
   */
  var loadModulsMTLG = function(newOptions) {
    modulsMTLG = modulsMTLG || {};
    //Override local settings if existing
    for (var prop in newOptions) {
      modulsMTLG[prop] = newOptions[prop];
    }
  };

  /**
   * @function getPlayerNumber -
   * This function returns the number of players
   * @memberof! MTLG
   */
  var getPlayerNumber = function() {
    return players.length;
  };

  /**
   * @function addPlayer -
   * This function adds a player and returns its index
   * @param newPlayer - the player to be added
   * @memberof! MTLG
   */
  var addPlayer = function(newPlayer) {
    players.push(newPlayer);
    return players.lastIndexOf;
  };

  /**
   * @function getPlayer -
   * This function returns a specific player
   * @param {number} index - the player's index
   * @memberof! MTLG
   */
  var getPlayer = function(index) {
    return players[index];
  };

  /**
   * @function removeAllPlayers -
   * This function removes the active players from the array and thus from the framework
   * @memberof! MTLG
   */
  var removeAllPlayers = function() {
    players = [];
  }

  /**
   * @function getPlayerName -
   * This function returns the name of a specific player
   * @param {number} index - the player's index
   * @memberof! MTLG
   */
  var getPlayerName = function(index) {
    return players[index].name;
  };

  /**
   * @function getOptions -
   * This functions returns the options object
   * @memberof! MTLG
   */
  var getOptions = function() {
    return options;
  };

  /**
   * @function getSettings -
   * This functions returns the settings object
   * @return {object}  the stage
   * @memberof! MTLG
   */
  var getSettings = function() {
    return settings;
  };


  /**
   * var getModulsMTLG - returns the current module settings
   *
   * @return {object}  the module settings
   */
  var getModulsMTLG = function() {
    return modulsMTLG;
  }

  /**
   * @function calculateCoordinates -
   * This function transforms coordinates back from area coordinates to global coordinates
   * @param area - area in which is drawn
   * @param Ox - the reference position where x = 0
   * @param Oy - the reference position where y = 0
   * @memberof! MTLG
   * @deprecated This kind of area should no longer be used
   */
  var calculateCoordinates = function(area, x, y) {
    //Check if rotation matrix exists in area
    if (!area.toGlobalMatrix) {
      area.toGlobalMatrix = {};
      area.toGlobalMatrix.x1 = Math.cos(-area.rotation * (Math.PI / 180)); //Need to convert to rad
      area.toGlobalMatrix.x2 = Math.sin(-area.rotation * (Math.PI / 180)); //Negative alpha because positive rotation is rotating clockwise
      area.toGlobalMatrix.y1 = -Math.sin(-area.rotation * (Math.PI / 180)); //Need to rotate back (counter-clockwise)
      area.toGlobalMatrix.y2 = Math.cos(-area.rotation * (Math.PI / 180)); //Thus it transforms a vector in area rotation to global rotation
    }
    return {
      get xKoord() {
        deprecation_warning("coord.xKoord", "coord.xCoord");
        return this.xCoord;
      },
      set xKoord(v) {
        deprecation_warning("coord.xKoord", "coord.xCoord");
        this.xCoord = v;
      },
      xCoord: area.toGlobalMatrix.x1 * x + area.toGlobalMatrix.x2 * y + area.Ox,
      get yKoord() {
        deprecation_warning("coord.yKoord", "coord.yCoord");
        return this.yCoord;
      },
      set yKoord(v) {
        deprecation_warning("coord.yKoord", "coord.yCoord");
        this.yCoord = v;
      },
      yCoord: area.toGlobalMatrix.y1 * x + area.toGlobalMatrix.y2 * y + area.Oy
    };
  };

  /**
   * @function toLocal -
   * This function transforms global to local coordinates for areas
   * @param area - area in which is drawn
   * @param Ox - the reference position where x = 0
   * @param Oy - the reference position where y = 0
   * @memberof! MTLG
   * @deprecated This kind of area should no longer be used
   */
  var toLocal = function(area, x, y) {
    //Check if rotation matrix exists in area
    if (!area.toLocalMatrix) {
      area.toLocalMatrix = {};
      area.toLocalMatrix.x1 = Math.cos(area.rotation * (Math.PI / 180)); //Need to convert to rad
      area.toLocalMatrix.x2 = Math.sin(area.rotation * (Math.PI / 180)); //Positive alpha because positive rotation is rotating clockwise
      area.toLocalMatrix.y1 = -Math.sin(area.rotation * (Math.PI / 180));
      area.toLocalMatrix.y2 = Math.cos(area.rotation * (Math.PI / 180)); //Thus it transforms a vector in global rotation to local rotation
    }
    //global Coord - upper left to receive vector from UL to global, then rotate that vector to receive coords in local
    return {
      get xKoord() {
        deprecation_warning("coord.xKoord", "coord.xCoord");
        return this.xCoord;
      },
      set xKoord(v) {
        deprecation_warning("coord.xKoord", "coord.xCoord");
        this.xCoord = v;
      },
      xCoord: area.toLocalMatrix.x1 * (x - area.Ox) + area.toLocalMatrix.x2 * (y - area.Oy),
      get yKoord() {
        deprecation_warning("coord.yKoord", "coord.yCoord");
        return this.yCoord;
      },
      set yKoord(v) {
        deprecation_warning("coord.yKoord", "coord.yCoord");
        this.yCoord = v;
      },
      yCoord: area.toLocalMatrix.y1 * (x - area.Ox) + area.toLocalMatrix.y2 * (y - area.Oy)
    };
  }

  /**
   * @function scaleUser
   * This function scales the given user area to (not by) the given scaling- factor </br>
   * TODO: Zur Zeit skaliert es um die übergeben Prozentzahl </br>
   * Assertions: area ist rechteckig, von der Mitte weg gedreht
   * @param area - the area that will be resized
   * @param factor - the factor by which should be scaled
   * @deprecated This kind of area should no longer be used
   * @memberof! MTLG
   */
  var scaleUser = function(area, factor) {
    //Idea:
    //First shrink into bottom left direction → is the centre (moved to the right) now farer from the middle?, yes:translate, no:keep
    if (!area) return undefined; //empty areas

    var scale = Math.sqrt(factor); //Each side of the area is scaled by this factor
    var oldWidth = area.width;
    var oldHeight = area.height;
    area.width = area.width * scale;
    area.height = area.height * scale;

    var newO = calculateCoordinates(area, 0, oldHeight - area.height);
    area.Ox = newO.xCoord; //Set these first so that calculateCoordinates works
    area.Oy = newO.yCoord;
    var middle1 = calculateCoordinates(area, area.width / 2, area.height / 2);
    middle1.xCoord -= options.width / 2;
    middle1.yCoord -= options.height / 2;
    var middle2 = calculateCoordinates(area, (oldWidth - area.width) + area.width / 2, area.height / 2);
    middle2.xCoord -= options.width / 2;
    middle2.yCoord -= options.height / 2;
    var length1 = Math.sqrt(Math.pow(middle1.xCoord, 2) + Math.pow(middle1.yCoord, 2));
    var length2 = Math.sqrt(Math.pow(middle2.xCoord, 2) + Math.pow(middle2.yCoord, 2));
    if (length2 > length1) {
      var newO = calculateCoordinates(area, oldWidth - area.width, 0);
      area.Ox = newO.xCoord;
      area.Oy = newO.yCoord;
    }
  }

  /**
   * @function getBiggestFreeArea -
   * This function calculates the largest possible free area on the stage,
   * which can be placed in the middle,
   * considering the specified areas
   * @param areas - the areas that must not intersect the new area
   * @deprecated This kind of area should no longer be used
   * @memberof! MTLG
   */
  var getBiggestFreeArea = function(areas) {
    var biggestFree = {
      xl: 0,
      xr: options.width,
      yu: 0,
      yl: options.height
    };
    var middle = {
      x: options.width / 2,
      y: options.height / 2
    };
    var comp1;
    var comp2;

    //Cut away from biggest Free
    var i = 0;
    //Each loop step looks at one area and cuts on one axis to ensure that the area is not part of biggestFree
    for (i = 0; i < areas.length; i++) {
      var area = areas[i];
      comp1 = calculateCoordinates(area, area.width / 2, area.height / 2);
      comp2 = calculateCoordinates(area, area.width, area.height);
      if (Math.abs(comp1.xCoord - middle.x) > Math.abs(comp1.yCoord - middle.y)) { //If comp1 is closer to middle on y-axis we cut on x-axis
        if (comp1.xCoord > middle.x) { //Right
          for (var j = 0; j < 2; j++) {
            for (var k = 0; k < 2; k++) {
              if (biggestFree.xr > calculateCoordinates(area, j * area.width, k * area.height).xCoord) {
                biggestFree.xr = calculateCoordinates(area, j * area.width, k * area.height).xCoord;
              }
            }
          }
        }
        if (comp1.xCoord < middle.x) { //Left
          for (var j = 0; j < 2; j++) {
            for (var k = 0; k < 2; k++) {
              if (biggestFree.xl < calculateCoordinates(area, j * area.width, k * area.height).xCoord) {
                biggestFree.xl = calculateCoordinates(area, j * area.width, k * area.height).xCoord;
              }
            }
          }
        }
        //TODO: Dont cut anything if area is right in the center?
      } else { //comp1 is closer to x axis
        if (comp1.yCoord < middle.y) { //Upper
          for (var j = 0; j < 2; j++) {
            for (var k = 0; k < 2; k++) {
              if (biggestFree.yu < calculateCoordinates(area, j * area.width, k * area.height).yCoord) {
                biggestFree.yu = calculateCoordinates(area, j * area.width, k * area.height).yCoord;
              }
            }
          }
        }
        if (comp1.yCoord > middle.y) { //Lower
          for (var j = 0; j < 2; j++) {
            for (var k = 0; k < 2; k++) {
              if (biggestFree.yl > calculateCoordinates(area, j * area.width, k * area.height).yCoord) {
                biggestFree.yl = calculateCoordinates(area, j * area.width, k * area.height).yCoord;
              }
            }
          }
        }
      }
    }

    return biggestFree;
  }


  /**
   * Function to create user areas.
   * @param numberOfPlayers: number of areas created
   */
  function createAreas(numberOfPlayers) {
    var screenWidth = MTLG.getOptions().width; //ctx.width;
    var screenHeight = MTLG.getOptions().height; //ctx.height;
    var areas = new Array();

    // 0 = single, 1= double, 2= quadrants, 3= quadrants-rotated, 4= eight
    switch (numberOfPlayers) {
      case 1: //single user
        var screen1 = {
          Ox: 0,
          Oy: 0,
          rotation: 0,
          width: screenWidth,
          height: screenHeight,
          index: 0
        };
        areas.push(screen1);
        break;

      case 2: //two users
        var screen1 = {
          Ox: (screenWidth / 2),
          Oy: 0,
          rotation: 90,
          width: screenHeight,
          height: (screenWidth / 2),
          index: 0
        };
        var screen2 = {
          Ox: (screenWidth / 2),
          Oy: screenHeight,
          rotation: -90,
          width: screenHeight,
          height: (screenWidth / 2),
          index: 1
        };
        areas.push(screen1);
        areas.push(screen2);
        break;

      case 4: //four users
        var screen1 = {
          Ox: (screenWidth / 2),
          Oy: (screenHeight / 2),
          rotation: 180,
          width: (screenWidth / 2),
          height: (screenHeight / 2),
          index: 0
        };
        var screen2 = {
          Ox: screenWidth,
          Oy: (screenHeight / 2),
          rotation: 180,
          width: (screenWidth / 2),
          height: (screenHeight / 2),
          index: 1
        };
        var screen3 = {
          Ox: 0,
          Oy: (screenHeight / 2),
          rotation: 0,
          width: (screenWidth / 2),
          height: (screenHeight / 2),
          index: 2
        };
        var screen4 = {
          Ox: (screenWidth / 2),
          Oy: (screenHeight / 2),
          rotation: 0,
          width: (screenWidth / 2),
          height: (screenHeight / 2),
          index: 3
        };
        areas.push(screen1);
        areas.push(screen2);
        areas.push(screen3);
        areas.push(screen4);
        break;

      case 8: //8 users
        var areaHeight = screenHeight / (2 + (9 / 9) * (1 + Math.sqrt(2)));
        var areaWidth = (9 / 9) * areaHeight;
        var coordOff = areaWidth * Math.cos(Math.PI / 4);
        var screen1 = {
          Ox: (screenWidth / 2 - areaWidth / 2),
          Oy: (areaHeight),
          rotation: 135,
          width: areaWidth,
          height: areaHeight,
          index: 0
        };
        var screen2 = {
          Ox: (screenWidth / 2 + areaWidth / 2),
          Oy: (areaHeight),
          rotation: 180,
          width: areaWidth,
          height: areaHeight,
          index: 1
        };
        var screen3 = {
          Ox: (screenWidth / 2 + areaWidth / 2 + coordOff),
          Oy: (areaHeight + coordOff),
          rotation: 225,
          width: areaWidth,
          height: areaHeight,
          index: 2
        };
        var screen4 = {
          Ox: (screenWidth / 2 + areaWidth / 2 + coordOff),
          Oy: (screenHeight / 2 + areaWidth / 2),
          rotation: 270,
          width: areaWidth,
          height: areaHeight,
          index: 3
        };
        var screen5 = {
          Ox: (screenWidth / 2 + areaWidth / 2),
          Oy: (screenHeight - areaHeight),
          rotation: 315,
          width: areaWidth,
          height: areaHeight,
          index: 4
        };
        var screen6 = {
          Ox: (screenWidth / 2 - areaWidth / 2),
          Oy: (screenHeight - areaHeight),
          rotation: 0,
          width: areaWidth,
          height: areaHeight,
          index: 5
        };
        var screen7 = {
          Ox: (screenWidth / 2 - areaWidth / 2 - coordOff),
          Oy: (screenHeight - areaHeight - coordOff),
          rotation: 45,
          width: areaWidth,
          height: areaHeight,
          index: 6
        };
        var screen8 = {
          Ox: (screenWidth / 2 - areaWidth / 2 - coordOff),
          Oy: (screenHeight / 2 - areaWidth / 2),
          rotation: 90,
          width: areaWidth,
          height: areaHeight,
          index: 7
        };
        areas.push(screen1);
        areas.push(screen2);
        areas.push(screen3);
        areas.push(screen4);
        areas.push(screen5);
        areas.push(screen6);
        areas.push(screen7);
        areas.push(screen8);
        break;
      default:
        console.log("Currently unsupported number of players: " + numberOfPlayers);
        break;
    }
    for (area of areas) {
      Object.defineProperty(area, 'hoehe', {
        get: function() {
          deprecation_warning("area.hoehe", "area.height")
          return this.height
        },
        set: function(v) {
          deprecation_warning("area.hoehe", "area.height")
          this.height = v
        }
      })
      Object.defineProperty(area, 'breite', {
        get: function() {
          deprecation_warning("area.breite", "area.width")
          return this.width
        },
        set: function(v) {
          deprecation_warning("area.breite", "area.width")
          this.width = v
        }
      })
    }
    return areas;
  }

  /**
   * @function createContainers -
   * Function to create user areas as createjs containers.
   * @param {int} numberOfPlayers - number of areas created
   * @memberof! MTLG
   */
  function createContainers(numberOfPlayers) {
    var screenWidth = MTLG.getOptions().width;
    var screenHeight = MTLG.getOptions().height;
    var areas = new Array();

    switch (numberOfPlayers) {
      case 1: //single user
        var screen1 = new createjs.Container();
        screen1.setBounds(0, 0, screenWidth, screenHeight);
        screen1.regX = screenWidth / 2;
        screen1.regY = screenHeight / 2;
        screen1.x = screenWidth / 2;
        screen1.y = screenHeight / 2;
        screen1.index = 0;
        areas.push(screen1);
        break;

      case 2: //two users
        var screen1 = new createjs.Container();
        screen1.setBounds(0, 0, screenHeight, screenWidth / 2);
        screen1.regX = screenHeight / 2;
        screen1.regY = screenWidth / 4;
        screen1.rotation = 90;
        screen1.x = screenWidth / 4;
        screen1.y = screenHeight / 2;
        screen1.index = 0;

        var screen2 = new createjs.Container();
        screen2.setBounds(0, 0, screenHeight, screenWidth / 2);
        screen2.regX = screenHeight / 2;
        screen2.regY = screenWidth / 4;
        screen2.rotation = -90;
        screen2.x = screenWidth * 3 / 4;
        screen2.y = screenHeight / 2;
        screen2.index = 1;

        areas.push(screen1);
        areas.push(screen2);
        break;

      case 4: //four users
        var screen1 = new createjs.Container();
        screen1.setBounds(0, 0, screenWidth / 2, screenHeight / 2);
        screen1.regX = screenWidth / 4;
        screen1.regY = screenHeight / 4;
        screen1.rotation = 180;
        screen1.x = screenWidth / 4;
        screen1.y = screenHeight / 4;
        screen1.index = 0;

        var screen2 = new createjs.Container();
        screen2.setBounds(0, 0, screenWidth / 2, screenHeight / 2);
        screen2.regX = screenWidth / 4;
        screen2.regY = screenHeight / 4;
        screen2.rotation = 180;
        screen2.x = screenWidth * 3 / 4;
        screen2.y = screenHeight / 4;
        screen2.index = 1;

        var screen3 = new createjs.Container();
        screen3.setBounds(0, 0, screenWidth / 2, screenHeight / 2);
        screen3.regX = screenWidth / 4;
        screen3.regY = screenHeight / 4;
        screen3.rotation = 0;
        screen3.x = screenWidth / 4;
        screen3.y = screenHeight * 3 / 4;
        screen3.index = 2;

        var screen4 = new createjs.Container();
        screen4.setBounds(0, 0, screenWidth / 2, screenHeight / 2);
        screen4.regX = screenWidth / 4;
        screen4.regY = screenHeight / 4;
        screen4.rotation = 0;
        screen4.x = screenWidth * 3 / 4;
        screen4.y = screenHeight * 3 / 4;
        screen4.index = 3;

        areas.push(screen1);
        areas.push(screen2);
        areas.push(screen3);
        areas.push(screen4);
        break;

      case 8: //8 users
        var screen1 = new createjs.Container();
        screen1.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen1.regX = screenWidth / 8;
        screen1.regY = screenHeight / 4;
        screen1.rotation = 180;
        screen1.x = screenWidth / 8;
        screen1.y = screenHeight / 4;
        screen1.index = 0;

        var screen2 = new createjs.Container();
        screen2.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen2.regX = screenWidth / 8;
        screen2.regY = screenHeight / 4;
        screen2.rotation = 180;
        screen2.x = screenWidth * 3 / 8;
        screen2.y = screenHeight / 4;
        screen2.index = 1;

        var screen3 = new createjs.Container();
        screen3.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen3.regX = screenWidth / 8;
        screen3.regY = screenHeight / 4;
        screen3.rotation = 180;
        screen3.x = screenWidth * 5 / 8;
        screen3.y = screenHeight / 4;
        screen3.index = 2;

        var screen4 = new createjs.Container();
        screen4.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen4.regX = screenWidth / 8;
        screen4.regY = screenHeight / 4;
        screen4.rotation = 180;
        screen4.x = screenWidth * 7 / 8;
        screen4.y = screenHeight / 4;
        screen4.index = 3;

        var screen5 = new createjs.Container();
        screen5.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen5.regX = screenWidth / 8;
        screen5.regY = screenHeight / 4;
        screen5.rotation = 0;
        screen5.x = screenWidth * 7 / 8;
        screen5.y = screenHeight * 3 / 4;
        screen5.index = 4;

        var screen6 = new createjs.Container();
        screen6.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen6.regX = screenWidth / 8;
        screen6.regY = screenHeight / 4;
        screen6.rotation = 0;
        screen6.x = screenWidth * 5 / 8;
        screen6.y = screenHeight * 3 / 4;
        screen6.index = 5;

        var screen7 = new createjs.Container();
        screen7.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen7.regX = screenWidth / 8;
        screen7.regY = screenHeight / 4;
        screen7.rotation = 0;
        screen7.x = screenWidth * 3 / 8;
        screen7.y = screenHeight * 3 / 4;
        screen7.index = 6;

        var screen8 = new createjs.Container();
        screen8.setBounds(0, 0, screenWidth / 4, screenHeight / 2);
        screen8.regX = screenWidth / 8;
        screen8.regY = screenHeight / 4;
        screen8.rotation = 0;
        screen8.x = screenWidth / 8;
        screen8.y = screenHeight * 3 / 4;
        screen8.index = 7;

        areas.push(screen1);
        areas.push(screen2);
        areas.push(screen3);
        areas.push(screen4);
        areas.push(screen5);
        areas.push(screen6);
        areas.push(screen7);
        areas.push(screen8);
        break;
      default:
        console.log("Currently unsupported number of players: " + numberOfPlayers);
        break;
    }

    return areas;

  }


  /////////////////////////////////////



  //counts timespan without any interaction in seconds
  var inactivityCounter = 0;

  /*
   * This function defines, which interactions reset the inactivityCounter
   * and starts the interval that increments the inactivityCounter each second by one
   */
  var resetTimer_init = function() {
    (document.all) ? document.onmousemove = resetResetTimer: window.onmousemove = resetResetTimer;
    (document.all) ? document.onkeypress = resetResetTimer: window.onkeypress = resetResetTimer;
    (document.all) ? document.onclick = resetResetTimer: window.onclick = resetResetTimer;
    (document.all) ? document.ontouchmove = resetResetTimer: window.ontouchmove = resetResetTimer;
    (document.all) ? document.ontouchstart = resetResetTimer: window.ontouchstart = resetResetTimer;

    resetResetTimer();
    window.setInterval(function() {
      incrementInactivityCounter();
    }, 1000);
  };

  /*
   * Register the reset Timer with the MTLG framework.
   * This is in the same scope as MTLG.
   */
  addModule(resetTimer_init);

  /*
   * Resets the inactivityCounter back to 0
   */
  var resetResetTimer = function() {
    inactivityCounter = 0;
  };

  /*
   * Increments inactivityCounter and reacts on timeout when given
   */
  var incrementInactivityCounter = function() {
    inactivityCounter = parseInt(inactivityCounter) + 1;
    if (inactivityCounter === options.resetCountdown + 1) {
      Game.neustart_init();
      resetResetTimer();
    }
  };

  /**
   * @function setBackgroundImage - a function to set a background image on a seperate stage.
   * @param {string} src - The path of the background image to be set relative to the img/ folder
   * @memberof! MTLG
   */
  var setBackgroundImage = function(src) {
    let bgstage = backgroundStage;

    var bg = MTLG.assets.getBitmap('img/' + src);
    bgstage.addChild(bg);
    bgstage.update();
  };

  /**
   * @function setBackgroundImageFill - a function to set a background image on a seperate stage. Stretches picture to fit stage
   * @param {string} src - The path of the background image to be set relative to the img/ folder
   * @memberof! MTLG
   */
  var setBackgroundImageFill = function(src) {
    let bgstage = backgroundStage;

    var bg = MTLG.assets.getBitmap('img/' + src);
    bg.scaleX = MTLG.getOptions().width / bg.getBounds().width;
    bg.scaleY = MTLG.getOptions().height / bg.getBounds().height;
    bgstage.addChild(bg);
    bgstage.update();
  };

  /**
   * @function setBackgroundColor - a function to set a background color on a seperate stage.
   * @param {string} color - The color that will be set
   * @memberof! MTLG
   */
  var setBackgroundColor = function(color) {
    let c = new createjs.Shape();
    c.graphics.beginFill(color).drawRect(0, 0, options.width, options.height);
    backgroundStage.addChild(c);
    backgroundStage.update();
  };

  /**
   * @function setBackgroundContainer - a function to set a createjs container as background on a seperate stage.
   * @param {container} color - The container that will be set
   * @memberof! MTLG
   */
  var setBackgroundContainer = function(container) {
    backgroundStage.addChild(container);
    backgroundStage.update();
  };

  /**
   * @function clearBackground - deletes everything on the background stage
   * @memberof! MTLG
   */
  var clearBackground = function() {
    backgroundStage.removeAllChildren();
    backgroundStage.update();
  };

  /**
   * @function getBackgroundStage - returns the background stage
   * @memberof! MTLG
   */
  var getBackgroundStage = function() {
    return backgroundStage;
  };

  var tickerPause = function() {
    createjs.Ticker.setPaused(true);
  };

  var tickerStart = function() {
    createjs.Ticker.setPaused(false);
  };

  /**
   * @function getStageContainer - returns the container that is used to simulate the stage
   * @memberof! MTLG
   */
  var getStageContainer = function() {
    return stageContainer;
  };

  /**
   * @function getScaleFactor - returns the current scaling factor
   * @memberof! MTLG
   */
  var getScaleFactor = function() {
    return scaleFactor || 1;
  };

  /////////////////////////////////////

  return {
    init: mtlg_init,
    tickerPause,
    tickerStart,
    setBackgroundImage,
    setBackgroundImageFill,
    setBackgroundColor,
    clearBackground,
    getBackgroundStage,
    setBackgroundContainer,
    getScaleFactor,
    geheZumNaechstenAbschnitt: () => {
      deprecation_warning("MTLG.geheZumNaechstenAbschnitt()", "MTLG.lc.levelFinished()");
      return geheZumNaechstenAbschnitt()
    },
    zumHauptmenue: () => {
      deprecation_warning("MTLG.zumHauptmenue()", "MTLG.lc.goToMenu()");
      return MTLG.lc.goToMenu()
    },
    calculateCoordinates,
    berechneKoordinaten: (...args) => {
      deprecation_warning("MTLG.berechneKoordinaten()", "MTLG.calculateCoordinates()");
      return calculateCoordinates(...args)
    },
    toLocal,
    getStageContainer,
    getStage,
    gibStage: (...args) => {
      deprecation_warning("MTLG.gibStage()", "MTLG.getStage()");
      return getStage(...args)
    },
    clearStage,
    loadSettings,
    getSettings,
    loadModulsMTLG,
    getModulsMTLG,
    loadOptions,
    getOptions,
    gibOptionen: (...args) => {
      deprecation_warning("MTLG.gibOptionen()", "MTLG.getOptions()");
      return getOptions(...args)
    },
    addPlayer,
    neuerSpieler: (...args) => {
      deprecation_warning("MTLG.neuerSpieler()", "MTLG.addPlayer()");
      return addPlayer(...args)
    },
    getPlayerNumber,
    gibSpielerAnzahl: (...args) => {
      deprecation_warning("MTLG.gibSpielerAnzahl()", "MTLG.getPlayerNumber()");
      return getPlayerNumber(...args)
    },
    getPlayer,
    gibSpieler: (...args) => {
      deprecation_warning("MTLG.gibSpieler()", "MTLG.getPlayer()");
      return getPlayer(...args)
    },
    removeAllPlayers,
    entferneAlleSpieler: (...args) => {
      deprecation_warning("MTLG.entferneAlleSpieler()", "MTLG.removeAllPlayers()");
      return removeAllPlayers(...args)
    },
    getPlayerName,
    gibSpielerName: (...args) => {
      deprecation_warning("MTLG.gibSpielerName()", "MTLG.assets.getPlayerName()");
      return getPlayerName(...args)
    },
    resize: resize_canvas,
    fixedSizeCm,
    addGameInit,
    addModule,
    createAreas: (...args) => {
      deprecation_warning("MTLG.createAreas()", "MTLG.createContainers()");
      return createAreas(...args)
    },
    createContainers,
    bereicheErstellen: (...args) => {
      deprecation_warning("MTLG.bereicheErstellen()", "MTLG.createAreas()");
      return createAreas(...args)
    },
    spieleSprachspezifischenSound: () => {
      deprecation_warning("MTLG.spieleSprachspezifischenSound()", "MTLG.assets.playLangSound()");
    },
    spieleAllgemeinenSound: () => {
      deprecation_warning("MTLG.spieleAllgemeinenSound()", "MTLG.assets.playSound()");
    },
    scaleUser,
    skaliereUser: () => {
      deprecation_warning("MTLG.skaliereUser()", "MTLG.scaleUser()");
      return scaleUser(...args);
    },
    getBiggestFreeArea,
    gibFreienBereich: (...args) => {
      deprecation_warning("MTLG.gibFreienBereich()", "MTLG.getBiggestFreeArea()");
      return getBiggestFreeArea(...args);
    }
  };


})(); //end MTLG
